# GIT : Système de gestion de version

> * Auteur : Gwénaël LAURENT
> * Date : 20/09/2024

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

* [1. Les bases](1-les-bases-de-git.md)
* [2. Créer un dépôt distant sur Github](2-creer-depot-distant-github.md)
* [3. Créer un dépôt distant sur GitLab](3-creer-depot-distant-gitlab.md)
* [4. Le fichier `.gitignore`](4-fichier-.gitignore.md)
* [5. Les commandes fetch et merge](5-commandes-fetch-merge.md)
* [6. Gérer les commits locaux](6-gerer-les-commits-locaux.md)
* [7. Intérêt d'utiliser des branches](7-interet-des-branches.md)

[Installation de Git sur Windows](installer-git-windows.md)\
[Syntaxe Markdown](markdown_syntaxe.md) 