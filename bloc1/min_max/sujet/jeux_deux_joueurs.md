Les jeux à deux joueurs
=======================

* Auteurs : FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>
* Modifications : Gwénaël LAURENT & Olivier SEYS
* Licence : CC BY 4.0
* Date : 01/07/2019

Jeux bien connus,
[Othello](https://fr.wikipedia.org/wiki/Othello_%28jeu%29), les échecs,
les dames, [Tic-tac-toe](https://fr.wikipedia.org/wiki/Tic-tac-toe), le
[puissance 4](https://fr.wikipedia.org/wiki/Puissance_4), [le jeu de Nim](https://fr.wikipedia.org/wiki/Jeux_de_Nim)
et bien d'autres, ont pour point commun d'être des "*jeux à deux
joueurs au tour par tour*". Ils ont aussi en commun d'être des jeux *à
connaissance parfaite*, car, à tout moment, les deux joueurs possèdent
exactement la même connaissance de l'état du jeu. De plus ils ne font
pas intervenir le hasard. Ce sont ces jeux que nous allons étudier.

Ces jeux ont des points communs. Ainsi pour chacun, une partie est à
tout moment caractérisée par le *joueur courant* (le prochain qui doit
jouer) et par un état du jeu, que nous appellerons *situation courante*.
Il s'agit par exemple

-   de la configuration des pièces sur l'échiquier aux échecs,
-   du nombre de cailloux restant à prendre dans le jeu de Nim
-   etc ...

De plus, ces jeux partagent un mécanisme de déroulement des parties
commun. Ce sont uniquement les *règles du jeu* différentes d'un jeu à
l'autre qui font la différence, pas la mécanique du jeu. Celle-ci se
base principalement sur le fait que les joueurs jouent alternativement
des "coups de jeu" en fonction de ce qui est autorisé par les règles
du jeu. Ces différents coups font évoluer le jeu de situation en
situation, jusqu'à ce que l'on atteigne une situation de fin de
partie. Le déroulement d'une partie peut donc être décrit ainsi :


**Déroulement des jeux à deux joueurs**


 1.  installer le jeu, c'est-à-dire créer la *situation courante* initiale  
 2.  déterminer le premier *joueur courant*  
 3.  si le jeu n'est pas fini   
	 + si le *joueur courant* peut jouer dans la *situation courante*   
		 + le *joueur courant* joue  
           c'est-à-dire qu'il choisit un coup possible parmi les coups autorisés dans la *situation courante* du jeu. Chaque coup de jeu amène le jeu dans une nouvelle situation. Donc choisir un coup de jeu (= jouer) revient à choisir la prochaine *situation courante* parmi toutes celles possibles.
		 + mettre à jour la *situation courante* suite au choix du joueur  
	 + sinon  
		 +  ne rien faire (la *situation courante* ne change pas)  
	 + l'autre joueur devient le *joueur courant*  
     + recommencer en 3.   
 4.  sinon le jeu est fini    
    afficher le résultat (le joueur vainqueur ou partie nulle)

Ce mécanisme de jeu nous fournit un algorithme permettant de jouer aux jeux à deux joueurs qui nous concernent. 

	 
