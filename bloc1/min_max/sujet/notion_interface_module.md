# Notion d'interface de module (rapidement)

* Auteurs : FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>
* Modifications : Gwénaël LAURENT & Olivier SEYS
* Licence : CC BY 4.0
* Date : 01/07/2019

*(les codes ci-dessous sont dans le dossier [code/](./code/))*

On suppose que l'on veut de plusieurs manières pouvoir appliquer un traitement qui "réduise" une liste de chaînes de caractères en un entier.
On identifie deux fonctions nécessaires à `apply_reduce` (voir plus bas), elles constituent **l'interface** du module *reduce* :

 - `prepare` : prépare le travail en produisant une liste d'entiers à partir des chaînes de caractères 
 - `execute` : produit l'entier par réduction de la liste fournie par la préparation

Il existe plusieurs manières de mettre en &oelig;uvre cette réduction. Par exemple dans la suite on cherche à compter le nombre de `'a'` dans une liste de chaînes de caractères ou à connaître la longueur de la plus longue chaîne.
On va donc réaliser deux **implémentations** de cette interface et utiliser l'une ou l'autre à partir d'un même module d'exploitation (`apply_reduce`).

 
## module `reducer1.py`
Première implémentation
```python
# fonction outil spécifique à reducer1
def number_of_a(chaine):
	result = 0
	for c in chaine:
		if c == 'a':
			result = result + 1
	return result

# fonctions de "l'interface" reduce
def prepare(list):
	'''
	@param {list(chaine)}  list : la liste des chaînes à préparer
	@return {list(int)} : liste du nombre de 'a' dans chacune des chaines de list
	'''
    return [ number_of_a(chaine) for chaine in list]
	
def execute(list):
	'''
	@param {list(int)} list : une liste d'entiers
	@return {int} la somme des valeurs de list²
	'''
	return sum(list)
```

## module `reducer2.py`

Seconde implémentation
```python
# fonctions de "l'interface" reduce
def prepare(list):
	'''
	@param {list(chaine)}  list : la liste des chaînes à préparer
	@return {list(int)} : liste des longueurs des chaines de list
	'''
    return [ len(chaine) for chaine in list]
	
def execute(list):
	'''
	@param {list(int)} list : une liste d'entiers
	@return {int} la plus grande des valeurs de list²
	'''
	return max(list)
```


## module `apply_reduce.py`

On constate que dans les deux implémentations précédentes les signatures des fonctions sont bien les mêmes. Les **contrats** sont similaires.
On peut donc définir un autre module qui exploite ces fonctions, à partir de leur nom, et facilement passer d'un module à l'autre.

Nous allons examiner ci-dessous comment on peut exploiter les deux modules créés au sein du module `apply_reduce` déjà évoqué.

*Remarque* Dans certains langages (à objet en particulier et Java par exemple) cette notion d'**interface** est une composante du langage et il y a un contrôle 
du respect d'une interface par une implémentation (contrôle réalisé par le compilateur en Java).



```python
import reducer1 as reducer
# import reducer2 as reducer

def apply_reduce(list):
	'''
	@param {list(chaine)}  list : la liste des chaînes à manipuler
	@return {int} la valeur fournie par l'opération de reduction
	'''
	ready_to_transform = reducer.prepare(list)
	result = reducer.execute(ready_to_transform)
	return result
	
print( apply_reduce(["timoleon", "abracadabra", "banane", "abc"]) )
```

Il suffit de changer l'import pour utiliser l'implémentation du module `reducer2` à la place de celle du module `reducer1`.

## import dynamique 

On peut avoir une importation dynamique des modules (ici passés en paramètre de la ligne de commande) :

```python
if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        module = 'reducer1'
    else:
        module = sys.argv[1]
    reducer = __import__(module)
    
    print( apply_reduce(["timoleon", "abracadabra", "banane", "abc"]) )
```	

Ce qui donne à l'utilisation : 
```bash
$ python3 apply_reduce.py
8

$ python3 apply_reduce.py reducer2
11

```
