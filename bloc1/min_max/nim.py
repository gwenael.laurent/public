#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: Jeu de Nim
:author: Gwénaël LAURENT & Olivier SEYS
:date: 1er juiller 2019

On dispose un tas d'allumettes au milieu de la table.
Les (deux) joueurs ramassent tour à tour 2 ou 3 allumettes. Celui qui prend la dernière a gagné.
S'il reste une seule allumette le jeu est nul.

"""


"""
Constantes globales
"""
#Mode de jeux
HUMAIN_HUMAIN = 1
HUMAIN_ALEATOIRE = 2
HUMAIN_MINMAX = 3
MODE = HUMAIN_HUMAIN # mode par défaut
JOUEUR_ORDI = 2

import random

def mode_de_jeu() :
    '''
    Afficher le nom du jeu
    Choisir le mode de jeu HUMAIN_HUMAIN, HUMAIN_ALEATOIRE ou HUMAIN_MINMAX 
    
    :return: (None)
    '''
    
    global MODE
    
    mode = None    
    while (mode != HUMAIN_HUMAIN) and (mode != HUMAIN_ALEATOIRE) and (mode != HUMAIN_MINMAX) :
        # Saisie utilisateur du mode de jeu
        print()
        print("---------------------------------------")
        print("Jeu de Nim")
        print("Choix du mode de jeu :")
        print("   1 - HUMAIN_HUMAIN")
        print("   2 - HUMAIN_ALEATOIRE")
        print("   3 - HUMAIN_MINMAX")
        print("---------------------------------------")
        choix = input("Choix du jeu : (1) - (2) - (3) - (T) Terminer : ")        
        print()
        if choix.upper() == 'T':
            break
        elif choix == '1':
            mode = HUMAIN_HUMAIN           
        elif choix == '2':
            mode = HUMAIN_ALEATOIRE
        elif choix == '3':
            mode = HUMAIN_MINMAX
        MODE = mode    
    

def creer_situation_courante() :
    '''
    Installer le jeu, c'est-à-dire créer la situation courante initiale
    
    :return: (int) situation courante (= nombre d'allumettes restantes)
    '''
    
    nb_init_allum = 0
    while nb_init_allum < 8 :
        nb_init_allum = int(input("Saisissez le nombre total d'allumettes (minimum 8) : "))
    
    return nb_init_allum


def joueur_courant(ancien_joueur = 0) :
    '''
    Déterminer le joueur courant en fonction de l'ancien joueur

    :param ancien_joueur: (int) numéro du joueur 1 ou 2
                                si ancien_joueur == 0, on choisit aléatoirement un num de joueur
    :return: numéro du prochain joueur

    >>> joueur_courant() in range(1, 3)
    True
    >>> joueur_courant(1)
    2
    >>> joueur_courant(2)
    1
    
    '''
    if not ancien_joueur :
        #choisir aléatoirement un numéro de joueur entre 1 et 2
        return random.randrange(1, 3)
    else :
        #prochain joueur entre numéro 1 et 2
        return (ancien_joueur % 2) + 1
    

def jeu_est_fini(sit) :
    '''
    Prédicat qui indique si le jeu est fini ou non
    Parce qu'on ne peut plus jouer
    ou parce qu'il y a un gagnant
    
    :param sit: (int) situation courante
    :return: (bool) True si jeu fini
                    False si le jeu n'est pas fini

    >>> jeu_est_fini(2)
    False
    >>> jeu_est_fini(1)
    True
    >>> jeu_est_fini(0)
    True

    '''
    if sit <= 1 : 
        return True
    else :
        return False


def joueur_peut_jouer(sit, num_joueur) :
    '''
    Prédicat qui indique si le joueur courant peut jouer dans la situation courante
    
    :param sit: (int) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (bool) True si le joueur peut jouer
    
    >>> joueur_peut_jouer(2, 1)
    True
    >>> joueur_peut_jouer(1, 1)
    False

    '''
    #num_joueur n'intervient pas pour le jeu de Nim
    
    if sit >= 2 :
        return True
    else :
        return False


def afficher_situation(sit) :
    '''
    Affiche la situation courante
    
    :param sit: (int) situation courante
    :return: (None)
    
    >>> afficher_situation(15)
    Nombre d'allumettes restant = 15

    '''
    print("Nombre d'allumettes restant = {}".format(sit))

def choisir_coup_humain(sit, num_joueur) :
    '''
    Attendre la saisie du joueur humain avec input()
    Vérifier que le coup saisi est autorisé dans la situation courante du jeu
    
    :param sit: (int) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (int) le coup choisi par le joueur
    '''
    coup_choisi = 0
    while (coup_choisi not in range(2, 4)) or (coup_choisi > sit) :
        coup_choisi = int(input("Joueur {}, combien d'allumettes ? 2 ou 3 ? ".format(num_joueur)))       
    return coup_choisi

def choisir_coup_alea(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: (int) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (int) le coup choisi par le joueur 
    '''

def choisir_coup_minmax(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: (int) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (int) le coup choisi par le joueur 
    '''

def choisir_coup(sit, num_joueur) :
    '''
    Suivant le mode de jeu choisi et le type de joueur, appel de la fonction
    choisir_coup_humain,
    choisir_coup_alea ou
    choisir_coup_minmax
    
    :param sit: (int) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (int) le coup choisi par le joueur
    '''
    if MODE == HUMAIN_HUMAIN :
        coup_choisi = choisir_coup_humain(sit, num_joueur)
        
    elif MODE == HUMAIN_ALEATOIRE :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_alea(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    elif MODE == HUMAIN_MINMAX :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_minmax(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    return coup_choisi




    


def maj_situation_courante(sit, coup) :
    '''
    Met à jour la situation courante en fonction du coup choisi par le joueur
    
    :param sit: (int) situation courante
    :param coup: (int) le coup choisi par le joueur
    :return: (int) nouvelle situation courante
    
    >>> maj_situation_courante(20, 3)
    17

    '''
    return sit - coup

def trouver_vainqueur(sit, num_prochain_joueur) :
    '''
    Trouver le joueur vainqueur si la partie n'est pas nulle.
    Le vainqueur est le joueur qui a joué AVANT num_prochain_joueur.
    
    :param sit: (int) situation courante
    :param num_prochain_joueur: (int) numéro du prochain joueur
    :return: (int) numéro du vainqueur ou 0 pour partie nulle
    
    >>> trouver_vainqueur(1, 1)
    0
    >>> trouver_vainqueur(0, 1)
    2
    >>> trouver_vainqueur(0, 2)
    1

    '''
    if sit == 1 :
        # partie nulle
        return 0
    else :
        # Le vainqueur est le joueur qui a joué AVANT num_prochain_joueur
        return (num_prochain_joueur % 2) + 1


def afficher_resultat_final(sit, num_vainqueur) :
    '''
    Affiche la situation après le dernier coup
    Affiche le joueur vainqueur ou partie nulle
    
    :param sit: () situation courante
    :return: (None)
    
    >>> afficher_resultat_final(1, 0)
    Nombre d'allumettes restant = 1
    Partie nulle
    >>> afficher_resultat_final(0, 2)
    Nombre d'allumettes restant = 0
    Le joueur 2 a gagné !!!!

    '''
    afficher_situation(sit)
    
    if num_vainqueur == 0 :
        print("Partie nulle")
    else :
        print("Le joueur {} a gagné !!!!".format(num_vainqueur))
    



if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
