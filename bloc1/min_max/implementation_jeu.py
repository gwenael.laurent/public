#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: Jeu Tic-tac-toe
:author: Gwénaël LAURENT & Olivier SEYS
:date: 1er juiller 2019
"""

"""
Constantes globales
"""
#Mode de jeux
HUMAIN_HUMAIN = 1
HUMAIN_ALEATOIRE = 2
HUMAIN_MINMAX = 3
MODE = HUMAIN_HUMAIN # mode par défaut
JOUEUR_ORDI = 2

import random

def mode_de_jeu() :
    '''
    Afficher le nom du jeu
    Choisir le mode de jeu HUMAIN_HUMAIN, HUMAIN_ALEATOIRE ou HUMAIN_MINMAX 
    
    :return: (None)
    '''
    global MODE
    
    mode = None    
    while (mode != HUMAIN_HUMAIN) and (mode != HUMAIN_ALEATOIRE) and (mode != HUMAIN_MINMAX) :
        # Saisie utilisateur du mode de jeu
        print()
        print("---------------------------------------")
        print("Jeu de ...")
        print("Choix du mode de jeu :")
        print("   1 - HUMAIN_HUMAIN")
        print("   2 - HUMAIN_ALEATOIRE")
        print("   3 - HUMAIN_MINMAX")
        print("---------------------------------------")
        choix = input("Choix du jeu : (1) - (2) - (3) - (T) Terminer : ")        
        print()
        if choix.upper() == 'T':
            break
        elif choix == '1':
            mode = HUMAIN_HUMAIN           
        elif choix == '2':
            mode = HUMAIN_ALEATOIRE
        elif choix == '3':
            mode = HUMAIN_MINMAX
        MODE = mode


def creer_situation_courante() :
    '''
    Installer le jeu, c'est-à-dire créer la situation courante initiale
    
    :return: situation courante
    '''
    return


def joueur_courant(ancien_joueur = 0) :
    '''
    Déterminer le joueur courant en fonction de l'ancien joueur

    :param ancien_joueur: (int) numéro du joueur 1 ou 2
                                si ancien_joueur == 0, on choisit aléatoirement un num de joueur
    :return: numéro du prochain joueur

    >>> joueur_courant() in range(1, 3)
    True
    >>> joueur_courant(1)
    2
    >>> joueur_courant(2)
    1
    
    '''
    if not ancien_joueur :
        #choisir aléatoirement un numéro de joueur entre 1 et 2
        return random.randrange(1, 3)
    else :
        #prochain joueur entre numéro 1 et 2
        return (ancien_joueur % 2) + 1

def jeu_est_fini(sit) :
    '''
    Prédicat qui indique si le jeu est fini ou non
    Parce qu'on ne peut plus jouer
    ou parce qu'il y a un gagnant
    
    :param sit: () situation courante
    :return: (bool) True si jeu fini
                    False si le jeu n'est pas fini
    '''
    return


def joueur_peut_jouer(sit, num_joueur) :
    '''
    Prédicat qui indique si le joueur courant peut jouer dans la situation courante
    
    :param sit: () situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (bool) True si le joueur peut jouer
    '''
    return

def afficher_situation(sit) :
    '''
    Affiche la situation courante
    
    :param sit: () situation courante
    :return: (None) 
    '''
    print(...)

def choisir_coup_humain(sit, num_joueur) :
    '''
    Attendre la saisie du joueur humain avec input()
    Vérifier que le coup saisi est autorisé dans la situation courante du jeu
    
    :param sit: () situation courante
    :param num_joueur: (int) numéro du joueur
    :return: () le coup choisi par le joueur 
    '''

def choisir_coup_alea(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: () situation courante
    :param num_joueur: (int) numéro du joueur
    :return: () le coup choisi par le joueur 
    '''

def choisir_coup_minmax(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: () situation courante
    :param num_joueur: (int) numéro du joueur
    :return: () le coup choisi par le joueur 
    '''

def choisir_coup(sit, num_joueur) :
    '''
    Suivant le mode de jeu choisi et le type de joueur, appel de la fonction
    choisir_coup_humain,
    choisir_coup_alea ou
    choisir_coup_minmax
    
    :param sit: () situation courante
    :param num_joueur: (int) numéro du joueur
    :return: () le coup choisi par le joueur
    '''
    if MODE == HUMAIN_HUMAIN :
        coup_choisi = choisir_coup_humain(sit, num_joueur)
        
    elif MODE == HUMAIN_ALEATOIRE :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_alea(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    elif MODE == HUMAIN_MINMAX :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_minmax(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    #return ...

def maj_situation_courante(sit, coup) :
    '''
    Met à jour la situation courante en fonction du coup choisi par le joueur
    
    :param sit: () situation courante
    :param coup: () le coup choisi par le joueur
    :return: () nouvelle situation courante
    
    '''

def trouver_vainqueur(sit, num_prochain_joueur) :
    '''
    Trouver le joueur vainqueur si la partie n'est pas nulle.
    Le vainqueur est le joueur qui a joué AVANT num_prochain_joueur.
    
    :param sit: () situation courante
    :param num_prochain_joueur: (int) numéro du prochain joueur
    :return: (int) numéro du vainqueur ou 0 pour partie nulle
    '''
    
    return num_vainqueur


def afficher_resultat_final(sit, num_vainqueur) :
    '''
    Affiche la situation après le dernier coup
    Affiche le joueur vainqueur ou partie nulle
    
    :param sit: () situation courante
    :return: (None) 
    '''
    afficher_situation(sit)
    
    print(...)



if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
