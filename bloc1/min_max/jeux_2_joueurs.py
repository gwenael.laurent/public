#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: Interface des Jeux à deux joueurs
:author: Gwénaël LAURENT & Olivier SEYS
:date: 1er juiller 2019
"""
#import nim as jeu
#import tictactoe as jeu


def jeu_2_joueurs() :
    '''
    Lance le jeu choisi par l'import
    :return: (None)
    '''
    jeu.mode_de_jeu() 
    sit = jeu.creer_situation_courante() 
    num_joueur = jeu.joueur_courant()
    
    while not jeu.jeu_est_fini(sit) :
        if jeu.joueur_peut_jouer(sit, num_joueur) :
            jeu.afficher_situation(sit)
            coup = jeu.choisir_coup(sit, num_joueur) #  saisie pour humain + renvoie le coup du joueur     
            sit = jeu.maj_situation_courante(sit, coup) #nouvelle situation courante
        num_joueur = jeu.joueur_courant(num_joueur)
        
    num_vainqueur = jeu.trouver_vainqueur(sit, num_joueur)
    jeu.afficher_resultat_final(sit, num_vainqueur) #le joueur vainqueur ou partie nulle
    



if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
    
    while 1:
        print()
        print("---------------------------------------")
        print("Jeux à deux joueurs :")
        print("   1 - Jeu de Nim")
        print("   2 - Jeu Tic-Tac-Toe")
        print("---------------------------------------")
        choix = input("Choix du jeu : (1) - (2) - (T) Terminer : ")        
        print()
        if choix.upper() == 'T':
            break
        elif choix == '1':
            import nim as jeu
            jeu_2_joueurs()
        elif choix == '2':
            import tictactoe as jeu
            jeu_2_joueurs()
    
    