# Interface pour les  jeux à deux joueurs

* Auteurs : FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>
* Modifications : Gwénaël LAURENT & Olivier SEYS
* Licence : CC BY 4.0
* Date : 01/07/2019

## Algorithme

Rappelons l'algorithme proposé :

 1.  installer le jeu, c'est-à-dire créer la *situation courante* initiale  
 2.  déterminer le premier *joueur courant*  
 3.  si le jeu n'est pas fini   
	 + si le *joueur courant* peut jouer dans la *situation courante*   
		 + le *joueur courant* joue  
           c'est-à-dire qu'il choisit un coup possible parmi les coups autorisés dans la *situation courante* du jeu. Chaque coup de jeu amène le jeu dans une nouvelle situation. Donc choisir un coup de jeu (= jouer) revient à choisir la prochaine *situation courante* parmi toutes celles possibles.
		 + mettre à jour la *situation courante* suite au choix du joueur  
	 + sinon  
		 +  ne rien faire (la *situation courante* ne change pas)  
	 + l'autre joueur devient le *joueur courant*  
     + recommencer en 3.   
 4.  sinon le jeu est fini    
    afficher le résultat (le joueur vainqueur ou partie nulle)

Cet algorithme est le même pour tous ces jeux. **Les variations sont dues uniquement aux règles du jeu**. Elles ont pour rôle :

 A.  à l'étape 1, comment installer le jeu
 B.  à l'étape 2, le changement de joueur
 C.  à l'étape 3, quand le jeu se termine (c'est-à-dire quand la
     *situation courante* atteinte correspond à une fin de partie)
 D.  à l'étape 3, si le _joueur courant_ peut jouer dans la _situation courante_
 E.  à l'étape 3, le *joueur courant* joue un coup possible parmi les coups autorisés dans la *situation courante* du jeu
 F.  à l'étape 3, mettre à jour la *situation courante* suite au choix du joueur
 G.  à l'étape 4, qui gagne ou pas

Grâce à une analyse descendante, on peut donc **identifier pour chacun de ces points une fonction** qu'il
faudra implémenter de manières différentes pour chacun des jeux que
l'on souhaite programmer. Dans l'ordre des points ci-dessus :

 A.  créer la situation initiale du jeu : `creer_situation_courante`
 B.  changer de joueur courant : `joueur_courant`
 C.  déterminer quand le jeu est fini, c'est-à-dire quand une situation est finale `jeu_est_fini`
 D.  déterminer si un joueur peut jouer `joueur_peut_jouer`
 E.  proposer un coup possible `choisir_coup`
 F.  mettre à jour la situation courante `maj_situation_courante`
 G.  déterminer qui est vainqueur en fonction de la situation finale atteinte `trouver_vainqueur` et afficher le vainqueur `afficher_resultat_final`


On peut compléter ces fonctions par :

 -   une fonction pour afficher l'état du jeu, `afficher_situation`
 -   une fonction pour choisir le mode de jeu, `mode_de_jeu`

Il reste à identifier les paramètres de ces différentes méthodes, mais elles devront probablement au moins avoir comme paramètre la situation de jeu considérée. Le joueur courant devra sans doute être paramètre de certaines de ces fonctions.

Il faut donc pour chaque jeu, mettre en &oelig;uvre chacune de ces méthodes. Chaque jeu doit être défini dans un module à part. Ces modules offrent donc les mêmes fonctions mais avec des **implémentations** différentes.

Il sera également nécessaire de définir, en plus, pour chaque jeu, la structure de données permettant de représenter une situation du jeu, ainsi que d'éventuelles fonctions pour la manipulation de cette structure de données.
Par exemple, pour le jeu de Nim, une situation peut être simplement représentée par le nombre d'allumettes posées sur la table. La structure est évidemment plus complexe pour l'Othello ou le puissance 4.

Cependant si la programmation de l'algorithme de jeu ci-dessus s'appuie sur ces noms de fonctions, il sera "facile" de changer le jeu joué en modifiant le jeu importé.

	 
