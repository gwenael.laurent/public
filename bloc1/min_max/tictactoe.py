#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: Jeu Tic-tac-toe
:author: Gwénaël LAURENT & Olivier SEYS
:date: 1er juiller 2019
"""

"""
Constantes globales
"""
#Mode de jeux
HUMAIN_HUMAIN = 1
HUMAIN_ALEATOIRE = 2
HUMAIN_MINMAX = 3
MODE = HUMAIN_HUMAIN # mode par défaut
JOUEUR_ORDI = 2

# Valeurs dans la grille de jeu
VIDE = 0
CROIX = 1
ROND = 2

#dictionnaire pour associer les valeurs numériques enregistrées dans la grille aux num_joueur
VAL_JOUEUR = { 0 : VIDE,    # pour case vide
               1 : CROIX,   # pour joueur 1
               2 : ROND }   # pour joueur 2

#dictionnaire pour l'affichage de la grille
SYMB_AFF = { 0 : " ",   # pour case vide
             1 : "X",   # pour joueur 1
             2 : "O" }  # pour joueur 2

import random

def mode_de_jeu() :
    '''
    Afficher le nom du jeu
    Choisir le mode de jeu HUMAIN_HUMAIN, HUMAIN_ALEATOIRE ou HUMAIN_MINMAX 
    
    :return: (None)
    '''
    global MODE
    
    mode = None    
    while (mode != HUMAIN_HUMAIN) and (mode != HUMAIN_ALEATOIRE) and (mode != HUMAIN_MINMAX) :
        # Saisie utilisateur du mode de jeu
        print()
        print("---------------------------------------")
        print("Jeu Tic-Tac-Toe") 
        print("Choix du mode de jeu :")
        print("   1 - HUMAIN_HUMAIN")
        print("   2 - HUMAIN_ALEATOIRE")
        print("   3 - HUMAIN_MINMAX")
        print("---------------------------------------")
        choix = input("Choix du jeu : (1) - (2) - (3) - (T) Terminer : ")        
        print()
        if choix.upper() == 'T':
            break
        elif choix == '1':
            mode = HUMAIN_HUMAIN           
        elif choix == '2':
            mode = HUMAIN_ALEATOIRE
        elif choix == '3':
            mode = HUMAIN_MINMAX
        MODE = mode    


def creer_situation_courante() :
    '''
    Installer le jeu, c'est-à-dire créer la situation courante initiale
    
    :return: (list (list)) situation courante (grille du tictactoe 3*3)
    
    >>> creer_situation_courante()
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    '''
    
    return [[VIDE]*3 for _ in range(3)]


def joueur_courant(ancien_joueur = 0) :
    '''
    Déterminer le joueur courant en fonction de l'ancien joueur

    :param ancien_joueur: (int) numéro du joueur 1 ou 2
                                si ancien_joueur == 0, on choisit aléatoirement un num de joueur
    :return: numéro du prochain joueur

    >>> joueur_courant() in range(1, 3)
    True
    >>> joueur_courant(1)
    2
    >>> joueur_courant(2)
    1
    
    '''
    if not ancien_joueur :
        #choisir aléatoirement un numéro de joueur entre 1 et 2
        return random.randrange(1, 3)
    else :
        #prochain joueur entre numéro 1 et 2
        return (ancien_joueur % 2) + 1


def alignement_present(sit, num_joueur) :
    '''
    Fonction utilitaire pour chercher si le joueur num_joueur à réussi un alignement
    un alignement de 3 cases = 8 possibilités d'alignement
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (bool) True si un alignement trouvé
                    False si pas d'alignement trouvé
    
    >>> sit = [[1, 1, 0], [2, 1, 2], [0, 1, 0]]
    >>> alignement_present(sit, 1)
    True

    >>> sit = [[1, 1, 2], [2, 2, 2], [0, 0, 1]]
    >>> alignement_present(sit, 2)
    True

    >>> sit = [[1, 1, 2], [2, 2, 0], [2, 0, 1]]
    >>> alignement_present(sit, 2)
    True

    '''
    # un alignement de 3 cases = 8 possibilités
    
    # test des 3 lignes
    for l in sit :
        nb = len([c for c in l if c == VAL_JOUEUR[num_joueur]])
        if nb == 3 : return True
    
    # test des 3 colonnes
    for c in range(3) :
        nb = len([case for case in [sit[0][c], sit[1][c], sit[2][c]] if case == VAL_JOUEUR[num_joueur] ])
        if nb == 3 : return True
        
    # test des 2 diagonales
    nb = len([case for case in [sit[0][0], sit[1][1], sit[2][2]] if case == VAL_JOUEUR[num_joueur] ])
    if nb == 3 : return True   
    nb = len([case for case in [sit[0][2], sit[1][1], sit[2][0]] if case == VAL_JOUEUR[num_joueur] ])
    if nb == 3 : return True
    
    return False


def nb_cases_vides(sit) :
    '''
    Retourne le nombre de cases vides dans la situation courante
    
    :param sit: (list(list))  situation courante
    :return: (int) le nombre de cases vides
    
    >>> sit = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    >>> nb_cases_vides(sit)
    9

    >>> sit = [[0, 0, 0], [0, 1, 0], [2, 0, 0]]
    >>> nb_cases_vides(sit)
    7
    '''
    return len([c for l in sit for c in l if c == VIDE])


def jeu_est_fini(sit) :
    '''
    Prédicat qui indique si le jeu est fini ou non
    Parce qu'on ne peut plus jouer
    ou parce qu'il y a un gagnant
    
    :param sit: (list(list))  situation courante
    :return: (bool) True si jeu fini
                    False si le jeu n'est pas fini

    >>> sit = [[1, 1, 0], [2, 2, 2], [0, 0, 0]]
    >>> jeu_est_fini(sit)
    True

    >>> sit = [[1, 1, 2], [2, 2, 1], [1, 2, 1]]
    >>> jeu_est_fini(sit)
    True
    '''
    
    # le jeu est-il fini parce qu'il y a un gagnant ?
    if alignement_present(sit, 1) : return True
    if alignement_present(sit, 2) : return True

    # le jeu est-il fini parce qu'il ne reste plus de cases libres ?
    if nb_cases_vides(sit) == 0 : return True
        
    return False


def joueur_peut_jouer(sit, num_joueur) :
    '''
    Prédicat qui indique si le joueur courant peut jouer dans la situation courante
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (bool) True si le joueur peut jouer
    
    >>> sit = [[1, 0, 0], [2, 2, 0], [0, 0, 0]]
    >>> joueur_peut_jouer(sit, 1)
    True

    '''
    if nb_cases_vides(sit) == 0 :
        return False
    else :
        return True
    

def afficher_situation(sit) :
    '''
    Affiche la situation courante
    
    :param sit: (list(list)) situation courante
    :return: (None)
    
    >>> sit = [[1, 0, 0], [2, 2, 0], [0, 0, 0]]
    >>> afficher_situation(sit)
          X |   |   
         ---|---|---
          O | O |   
         ---|---|---
            |   |
            
    '''
    print("      {} | {} | {} ".format(SYMB_AFF[sit[0][0]], SYMB_AFF[sit[0][1]], SYMB_AFF[sit[0][2]]))
    print("     ---|---|---")
    print("      {} | {} | {} ".format(SYMB_AFF[sit[1][0]], SYMB_AFF[sit[1][1]], SYMB_AFF[sit[1][2]]))
    print("     ---|---|---")
    print("      {} | {} | {} ".format(SYMB_AFF[sit[2][0]], SYMB_AFF[sit[2][1]], SYMB_AFF[sit[2][2]]))


def choisir_coup_humain(sit, num_joueur) :
    '''
    Attendre la saisie du joueur humain avec input()
    Vérifier que le coup saisi est autorisé dans la situation courante du jeu
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (tuple) le coup choisi par le joueur (num_joueur, ligne, colonne)
    '''
    coup_choisi = (5, 5) #valeur volontairement fausse
    # Tester si la colonne et la ligne choisies sont dans la grille de jeu
    # ET que la case choisie est bien vide
    # Si la case choisie n'est pas bonne, on demande à en choisir une autre
    while (coup_choisi[0] not in range(3)) or (coup_choisi[1] not in range(3)) or (sit[coup_choisi[0]][coup_choisi[1]] != VIDE) :
        # Saisie utilisateur d'une case
        liste_str = input("Joueur {} avec les '{}' : [ligne,colonne] ? ".format(num_joueur, SYMB_AFF[num_joueur])).split(',')
        coup_choisi = (int(liste_str[0]), int(liste_str[1]))
    return (num_joueur, coup_choisi[0], coup_choisi[1])

def choisir_coup_alea(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (tuple) le coup choisi par le joueur (num_joueur, ligne, colonne)
    '''

def choisir_coup_minmax(sit, num_joueur) :
    '''
    Choisit aléatoirement une case parmi celles autorisés dans la situation courante du jeu.
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (tuple) le coup choisi par le joueur (num_joueur, ligne, colonne)
    '''

def choisir_coup(sit, num_joueur) :
    '''
    Suivant le mode de jeu choisi et le type de joueur, appel de la fonction
    choisir_coup_humain,
    choisir_coup_alea ou
    choisir_coup_minmax
    
    :param sit: (list(list)) situation courante
    :param num_joueur: (int) numéro du joueur
    :return: (tuple) le coup choisi par le joueur (num_joueur, ligne, colonne)
    '''
     
    if MODE == HUMAIN_HUMAIN :
        coup_choisi = choisir_coup_humain(sit, num_joueur)
        
    elif MODE == HUMAIN_ALEATOIRE :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_alea(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    elif MODE == HUMAIN_MINMAX :
        if num_joueur == JOUEUR_ORDI :
            coup_choisi = choisir_coup_minmax(sit, num_joueur)
        else :
            coup_choisi = choisir_coup_humain(sit, num_joueur)
            
    return (coup_choisi[0], coup_choisi[1], coup_choisi[2])


def maj_situation_courante(sit, coup) :
    '''
    Met à jour la situation courante en fonction du coup choisi par le joueur
    
    :param sit: (list(list)) situation courante
    :param coup: (tuple) le coup choisi par le joueur (num_joueur, ligne, colonne)
    :return: (list(list)) nouvelle situation courante

    >>> sit = [[1, 0, 0], [2, 2, 0], [0, 0, 0]]
    >>> maj_situation_courante(sit,(1, 0, 2))
    [[1, 0, 1], [2, 2, 0], [0, 0, 0]]

    '''
    sit[coup[1]][coup[2]] = VAL_JOUEUR[coup[0]]
    return sit


def trouver_vainqueur(sit, num_prochain_joueur) :
    '''
    Trouver le joueur vainqueur si la partie n'est pas nulle.
    Le vainqueur est le joueur qui a joué AVANT num_prochain_joueur.
    
    :param sit: (list(list)) situation courante
    :param num_prochain_joueur: (int) numéro du prochain joueur
    :return: (int) numéro du vainqueur ou 0 pour partie nulle
    
    >>> sit = [[1, 0, 0], [2, 2, 2], [0, 0, 0]]
    >>> trouver_vainqueur(sit, 1)
    2

    '''
    # le jeu est-il fini parce qu'il y a un gagnant ?
    if alignement_present(sit, 1) :
        return 1
    elif alignement_present(sit, 2) :
        return 2   
    elif nb_cases_vides(sit) == 0 :
        # le jeu est-il fini parce qu'il ne reste plus de cases libres ?
        return 0


def afficher_resultat_final(sit, num_vainqueur) :
    '''
    Affiche la situation après le dernier coup
    Affiche le joueur vainqueur ou partie nulle
    
    :param sit: (list(list)) situation courante
    :return: (None)
    
    >>> sit = [[1, 0, 0], [2, 2, 2], [0, 0, 0]]
    >>> afficher_resultat_final(sit, 2)
          X |   |   
         ---|---|---
          O | O | O 
         ---|---|---
            |   |   
    Le joueur 2 a gagné avec les 'O' !!!!

    '''
    afficher_situation(sit)
    
    if num_vainqueur == 0 :
        print("Partie nulle")
    else :
        print("Le joueur {} a gagné avec les '{}' !!!!".format(num_vainqueur, SYMB_AFF[num_vainqueur]))



if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
    #sit = creer_situation_courante()
