# TP Jeux à deux joueurs


* Auteurs : Gwénaël LAURENT & Olivier SEYS
* Inspiré de : FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>
* Licence : CC BY 4.0
* Date : 01/07/2019

# 1. Objectifs du TP

Mettre en oeuvre un ensemble de modules qui permettent de jouer aux jeux à deux joueurs 
avec au moins deux jeux proposés :
* le **jeu de Nim** qui facilite les tests par sa simplicité
* le jeu **Tic-tac-toe** 
* (vous pourrez poursuivre avec Puissance 4, Othello, etc).

Dans cette version du TP il s'agit de jeux :
* *humain contre humain*
* *humain contre ordinateur bête (mode aléatoire)*
  

# 1. Présentation des jeux à deux joueurs

**Exemples :**
* les échecs, les dames, le go, awele, puissance 4, othello, tic-tac-toe, etc

**Caractéristiques de ces jeux :**
* 2 joueurs jouant en alternance
* jeux à information complète
  - les joueurs ont toute l'information
  - les joueurs ont la même information
* jeux à somme nulle
  - les intérêts des joueurs sont opposés
  - la somme des gains est nulle : ce qui est gagné par l'un est perdu par l'autre

> Consultez la présentation plus complète de ces jeux, ainsi que l'algorithme principal de tous ces jeux : "[Les jeux à deux joueurs](./sujet/jeux_deux_joueurs.md)"


# 2. Dégager l'interface pour les jeux

L'algorithme est le même pour tous les jeux à deux joueurs. Les variations sont dues uniquement aux règles du jeu.

 1.  installer le jeu, c'est-à-dire créer la *situation courante* initiale  
 2.  déterminer le premier *joueur courant*  
 3.  si le jeu n'est pas fini   
	 + si le *joueur courant* peut jouer dans la *situation courante*   
		 + le *joueur courant* joue  
           c'est-à-dire qu'il choisit un coup possible parmi les coups autorisés dans la *situation courante* du jeu. Chaque coup de jeu amène le jeu dans une nouvelle situation. Donc choisir un coup de jeu (= jouer) revient à choisir la prochaine *situation courante* parmi toutes celles possibles.
		 + mettre à jour la *situation courante* suite au choix du joueur  
	 + sinon  
		 +  ne rien faire (la *situation courante* ne change pas)  
	 + l'autre joueur devient le *joueur courant*  
     + recommencer en 3.   
 4.  sinon le jeu est fini    
    afficher le résultat (le joueur vainqueur ou partie nulle)

> Une analyse de cet algorithme doit permettre d'identifier les **fonctions à définir** pour n'importe quel jeu à deux joueurs.

Une des premières tâches que vous devez réaliser est donc cette analyse afin de produire **l'interface** des jeux à deux joueurs :
* identifier les profils des fonctions à définir (on parle de *signature* de fonction)
* Il doit être possible de changer le jeu utilisé simplement en modifiant un `import` dans le fichier interface :
  - Pour le jeu de Nim `import nim as jeu`
  - Pour le jeu Tic-tac-toe `import tictactoe as jeu`

> Consultez la présentation des interfaces de module et de leurs implémentations : "[Notion d'interface de module](./sujet/notion_interface_module.md)"

**Enveloppe de secours** 
Si vous êtes bloqués (après une réflexion suffisante) ou si vous souhaitez comparer votre analyse, nous vous proposons [cette aide](./aide/enveloppe_secours_analyse.md).


# 3. Codage de l'implémentation du jeu de Nim (Mode humain-humain uniquement)

Les règles du jeu sont simples :

 > On dispose un tas d'allumettes au milieu de la table.    
 > Les (deux) joueurs ramassent tour à tour 2 ou 3 allumettes. Celui qui prend la dernière a gagné.    
 > S'il reste une seule allumette le jeu est nul.

Il existe des stratégies gagnantes pour ce jeu, mais sa simplicité en fait un très bon candidat pour pour commencer votre implémentaion d'un jeu à 2 joueurs.

> Téléchargez le squelette des modules d'implémentation [`implementation_jeu.py`](./implementation_jeu.py)

Vous trouverez dans le squelette quelques fonctions utilitaires qui permettent de s'adapter au mode de jeu choisi (_humain contre humain_ ou _humain contre ordinateur_) :
		* `choisir_coup_humain`
		* `choisir_coup_alea`



# 4. Codage de l'implémentation du jeu Tic-tac-toe (Mode humain-humain uniquement)

Réalisez l'implémentation du jeu Tic-tac-toe pour qu'il soit lancé **à partir de la même interface**.

Modifiez le fichier interface pour permettre au joueur 
* de choisir le jeu
* de recommencer une partie du même jeu ou d'un autre

Proposition d'affichage :
```python
---------------------------------------
Jeux à deux joueurs :
   1 - Jeu de Nim (humain contre humain)
   2 - Jeu Tic-Tac-Toe (humain contre humain)
---------------------------------------
Choix du jeu : (1) - (2) - (T) Terminer : 
```

```python
      O | O |   
     ---|---|---
      X | X |   
     ---|---|---
        |   |   
Joueur 2 avec les 'O' : [ligne,colonne] ? 0,2 
```

**Aide pour simplifier le codage**

Avant de coder la fonction `jeu_est_fini`, ajoutez une fonction prédicat utilitaire `alignement_present` qui prend en paramètre une situation courante et un numéro de joueur. Cette fonction renvoie `True` si le joueur a réussi un alignement dans la situation courante (ligne, colonne ou diagonale).

```python
>>> sit = [[1, 1, 0], [2, 1, 2], [0, 1, 0]]
>>> chercher_alignement(sit, 1)
True

>>> sit = [[1, 1, 2], [2, 2, 2], [0, 0, 1]]
>>> chercher_alignement(sit, 2)
True

>>> sit = [[1, 1, 2], [2, 2, 0], [2, 0, 1]]
>>> chercher_alignement(sit, 2)
True
```

Ajoutez également une fonction `nb_cases_vides` qui prend en paramètre une situation courante.
Cette fonction retourne le nombre de cases vides dans la situation courante.

```python
>>> sit = [[0, 0, 0], [0, 1, 0], [2, 0, 0]]
>>> nb_cases_vides(sit)
7
```

# 5. Modification du jeu de Nim : humain contre ordinateur bête (mode aléatoire)



# 6. Modification du jeu Tic-tac-toe : humain contre ordinateur bête (mode aléatoire)
