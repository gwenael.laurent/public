# Documentation technique pour les TP

> * Auteur : Gwénaël LAURENT (glaurent001@gmail.com)
> * Date : 11-03-2024

![CC-BY-NC-SA](img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

# 1. Logiciel pour la programmation
* [Visual Studio Code : Installation et utilisation](./vscode/readme.md)
* [IDE arduino](arduino-ide/readme.md)

# 2. Git et Gitlab
* [Utilisation de GitLab](gitlab/readme.md)

# 3. Javascript
* [Ressources pour la programmation Javascript](javascript/readme.md)

# 4. Slack et Zoom
* [Rejoindre un espace de travail Slack](slack/readme.md)
* [Installation de Zoom pour les visioconférences](zoom/readme.md)

# 5. Sigfox
* [SIGFOX SNOC breakout BRKWS01](sigfox/readme.md)
* [Utiliser Minicom sur Raspberry pour communiquer avec les cartes SIGFOX SNOC BRKWS01](sigfox/sigfox_minicom_raspi.md)

# 6. Arduino IDE
* [Install et config de l'IDE arduino](arduino-ide/readme.md)

# 7. PlatformIO : arduino, ESP8266, ESP32
* [VScode - Installation de PlatformIO (arduino, ESP8266, ESP32)](vscode/vscode-platformio.md)
* [Ressources pour la programmation embarquée avec PlatformIO](platformio/readme.md)