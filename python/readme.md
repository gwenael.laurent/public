# Ressources pour la programmation Python

> * Auteur : Gwénaël LAURENT
> * Date : 14/10/2024

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)



* [Installer l'interpréteur python sur Windows et configurer VScode](../vscode/vscode-python.md)

Les bases du langage Python :
* [01-Introduction à Python](01_introduction_python.md)
* [02-Principaux types de données](02_types_de_donnees.md)
* [03-Formater des chaînes de caractères](03_formater_les_chaines_de_caracteres.md)
* [04-Tests (Instructions conditionnelles)](04_tests_conditions.md)
* [05-Boucles](05_boucles.md)
* [06-Chaines de caractères en Python](06_chaines_de_caracteres.md)
* [07-Listes et range en Python](07_listes.md)
* [08-Tuples en Python](08_tuples.md)
* [09-Dictionnaires en Python](09_dictionnaires.md)
* [10-Fonctions en Python](10_fonctions.md)
* [11-Classes et objets](11_classes_et_objets.md)

Pour aller plus loin :
* [12-Modules et importations](12_modules_et_importations.md)
* [13-Utiliser les environnements virtuels Python](13_environnements_virtuels_Python.md)
* [14-Installer un module tiers avec `pip`](14_installer_un_module_tiers_avec_pip.md)
* [15-Gestion des erreurs en Python](15_gestion_des_erreurs.md)
* [16-Mesure et affichage du temps d'exécution](16_mesure_temps_execution.md)
* [17-Nombres pseudo-aléatoires en Python](17_nombres_pseudo_aleatoires.md)
* [18-Les Doctests en Python](18_doctests.md)
* [19a-Accéder au système de fichiers](19a_acceder_au_systeme_de_fichiers.md)
* [19b-Lire et écrire des fichiers avec pathlib](19b_lire_ecrire_des_fichiers.md)
* [20-Créer des graphiques avec matplotlib](20_graphique_matplotlib.md)