# Classes et objets

> * Auteur : David Cassagne - https://courspython.com/
> * Inspiré du livre de Gérard Swinnen [« Apprendre à programmer avec Python 3 »](http://inforef.be/swi/python.htm)
> * Adaptation : Gwénaël LAURENT
> * Date : 27/08/2024

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

- [Classes et objets](#classes-et-objets)
- [Brève introduction à la Programmation Orientée Objet](#brève-introduction-à-la-programmation-orientée-objet)
- [Les notions de classe et d’objet](#les-notions-de-classe-et-dobjet)
  - [Définition d’une classe `Point`](#définition-dune-classe-point)
  - [Création d’un objet de type `Point`](#création-dun-objet-de-type-point)
  - [Définition des attributs](#définition-des-attributs)
  - [Définition des méthodes](#définition-des-méthodes)
  - [La notion de constructeur](#la-notion-de-constructeur)
- [La notion d’encapsulation](#la-notion-dencapsulation)
  - [Définition d’attributs privés](#définition-dattributs-privés)
  - [Accesseurs et mutateurs](#accesseurs-et-mutateurs)
- [Attributs et méthodes **de classe**](#attributs-et-méthodes-de-classe)
  - [Attributs de classe](#attributs-de-classe)
  - [Méthodes de classe](#méthodes-de-classe)


Python est un langage qui permet la **Programmation Orientée Objet** (POO).

# Brève introduction à la Programmation Orientée Objet 

Nous avons vu plusieurs types de base en Python (`int` pour les entiers, `float` pour les flottants, `str` pour les chaînes de caractères, etc.). La notion de **classe** va en quelque sorte nous permettre de généraliser la notion de « type » afin de créer de nouvelles structures de données.

Une classe définit des **attributs** et des **méthodes**. Par exemple, imaginons une classe `Voiture` qui servira à créer des objets qui sont des voitures. Cette classe va pouvoir définir un attribut `couleur`, un attribut `vitesse`, etc. Ces attributs correspondent à des propriétés qui peuvent exister pour une voiture. La classe `Voiture` pourra également définir une méthode `rouler()`. Une méthode correspond en quelque sorte à une action, ici l’action de rouler peut être réalisée pour une voiture.

![img/classe_voiture.svg](img/classe_voiture.svg)

Si on imagine une classe `Avion`, elle pourra définir une méthode `voler()`. Elle pourra aussi définir une méthode `rouler()`. Par contre, la classe `Voiture` n’aura pas de méthode `voler()` car une voiture ne peut pas voler. De même, la classe `Avion` pourra avoir un attribut `altitude` mais ce ne sera pas le cas pour la classe `Voiture`.

Après avoir présenté la notion de **classe**, nous allons voir la notion d”**objet**. On dit qu’**un objet est une instance de classe**. Si on revient à la classe `Voiture`, nous pourrons avoir plusieurs voitures qui seront chacune des instances bien distinctes. Par exemple, la voiture de Jonathan, qui est de couleur rouge avec une vitesse de 30 km/h, est une instance de la classe `Voiture`, c’est un **objet**. De même, la voiture de Denis, qui est de couleur grise avec une vitesse de 50 km/h, est un autre objet. Nous pouvons donc avoir plusieurs objets pour une même classe, en particulier ici deux objets (autrement dit : deux instances de la même classe). Chacun des objets a des valeurs qui lui sont propres pour les attributs.

# Les notions de classe et d’objet 

## Définition d’une classe `Point` 

Voici comment définir une classe appelée ici `Point`.

```py
class Point:
    "Definition d'un point géométrique"

```
Par convention en Python, le nom identifiant une classe (qu’on appelle aussi son identifiant) débute par une majuscule. Ici `Point` débute par un **P** majuscule.

## Création d’un objet de type `Point` 

```py
Point()
```

Ceci crée un objet de type `Point`. En POO, on dit que l’on crée une **instance** de la classe `Point`.

Une phrase emblématique de la POO consiste à dire qu”**un objet est une instance de classe**.

Il faut bien noter que pour créer une instance, on utilise le nom de la classe suivi de parenthèses. Nous verrons par la suite qu’il peut y avoir des arguments entre ces parenthèses.

**Affectation à une variable de la référence à un objet**

Nous venons de définir une classe `Point`. Nous pouvons dès à présent nous en servir pour créer des objets de ce type, par instanciation. Créons par exemple un nouvel objet et mettons la référence à cet objet dans la variable `p` :

```py
>>> p = Point()
```

>**Avertissement :**
>
> Comme pour les fonctions, lors de l’appel à une classe dans une instruction pour créer un objet, il faut toujours indiquer des parenthèses (même si aucun argument n’est transmis). Nous verrons un peu plus loin que ces appels peuvent se faire avec des arguments (voir la notion de constructeur).
>
> Remarquez bien cependant que la définition d’une classe ne nécessite pas de parenthèses (contrairement à ce qui est de règle lors de la définition des fonctions), sauf si nous souhaitons que la classe en cours de définition dérive d’une autre classe préexistante.
>
> Nous pouvons dès à présent effectuer quelques manipulations élémentaires avec notre nouvel objet dont la référence est dans `p`.

**Exemple**

```py
>>> print(p) 
<__main__.Point object at 0x0000022909112190>
```

Le message renvoyé par Python indique que `p` contient une référence à une instance de la classe `Point`, qui est définie elle-même au niveau principal du programme. Elle est située dans un emplacement bien déterminé de la mémoire vive, dont l’adresse apparaît ici en notation hexadécimale.

```py
>>> print(p.__doc__) 
Definition d'un point géométrique
```

On peut noter que les chaînes de documentation de divers objets Python sont associées à l’attribut prédéfini `__doc__`:.

**Exemple avec deux objets**

```py
>>> a = Point()
>>> b = Point()
```

La variable `a` va contenir une référence à un objet.

```py
>>> print(a)
<__main__.Point object at 0x0000022909112090>
```

De même `b` va contenir une référence à un autre objet.

```py
>>> print(b)
<__main__.Point object at 0x0000022909112210>
```

Nous avons ici 2 instances de la classe `Point` (2 objets situés à 2 emplacements différents de la RAM ) :
*   la première à laquelle on fait référence au moyen de la variable `a`,
*   la seconde à laquelle on fait référence au moyen de la variable `b`.
    

On fait bien ici la distinction entre classe et objet. Ici nous avons une seule classe `Point`, et deux objets de type `Point`.

## Définition des attributs 

```py
class Point:
    "Definition d'un point geometrique"

p = Point()
p.x = 1
p.y = 2
print("p : x =", p.x, "y =", p.y)
```
Affichage après exécution :
```
p : x = 1 y = 2
```

[Exécuter dans Python Tutor](https://pythontutor.com/render.html#code=class%20Point%3A%0A%20%20%20%20%22Definition%20d'un%20point%20geometrique%22%0A%0Ap%20%3D%20Point%28%29%0Ap.x%20%3D%201%0Ap.y%20%3D%202%0Aprint%28%22p%20%3A%20x%20%3D%22,%20p.x,%20%22y%20%3D%22,%20p.y%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

L’objet dont la référence est dans `p` possède deux attributs : `x` et `y`.

![img/2014-11-22_13-30-47.png](img/2014-11-22_13-30-47.png)

La syntaxe pour accéder à un attribut est la suivante : on va utiliser la variable qui contient la référence à l’objet et on va mettre un point **`.`** puis le nom de l’attribut.

**Exemple**

```py
class Point:
    "Definition d'un point geometrique"

a = Point()
a.x = 1
a.y = 2
b = Point()
b.x = 3
b.y = 4
print("a : x =", a.x, "y =", a.y)
print("b : x =", b.x, "y =", b.y)
```
Affichage après exécution :
```
a : x = 1 y = 2
b : x = 3 y = 4
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++%22Definition+d'un+point+geometrique%22%0A%0Aa+%3D+Point()%0Aa.x+%3D+1%0Aa.y+%3D+2%0Ab+%3D+Point()%0Ab.x+%3D+3%0Ab.y+%3D+4%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aprint(%22b+%3A+x+%3D%22,+b.x,+%22y+%3D%22,+b.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

On a 2 instances de la classe `Point`, c’est-à-dire 2 objets de type `Point`. Pour chacun d’eux, les attributs prennent des valeurs qui sont propres à l’instance.

![img/2014-11-22_09-47-40.png](img/2014-11-22_09-47-40.png)

**Distinction entre variable et objet**

L’exemple suivant montre bien la distinction entre variable et objet :

```py
class Point:
    "Definition d'un point geometrique"

a = Point()
a.x = 1
a.y = 2
b = a
print("a : x =", a.x, "y =", a.y)
print("b : x =", b.x, "y =", b.y)
a.x = 3
a.y = 4
print("a : x =", a.x, "y =", a.y)
print("b : x =", b.x, "y =", b.y)
```

Affichage après exécution :
```
a : x = 1 y = 2
b : x = 1 y = 2
a : x = 3 y = 4
b : x = 3 y = 4
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++%22Definition+d'un+point+geometrique%22%0A%0Aa+%3D+Point()%0Aa.x+%3D+1%0Aa.y+%3D+2%0Ab+%3D+a%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aprint(%22b+%3A+x+%3D%22,+b.x,+%22y+%3D%22,+b.y)%0Aa.x+%3D+3%0Aa.y+%3D+4%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aprint(%22b+%3A+x+%3D%22,+b.x,+%22y+%3D%22,+b.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

![img/2014-11-22_09-56-48.png](img/2014-11-22_09-56-48.png)

Ici les variables `a` et `b` font référence au même objet. En effet, lors de l’affectation `b = a`, on met dans la variable `b` la référence contenue dans la variable `a`. Par conséquent, toute modification des valeurs des attributs de l’objet dont la référence est contenue dans `a` entraîne une modification pour `b`.

> **Avertissement :**
>
> Par abus de langage on parlera parfois de l’objet `a` alors qu’il s’agira en fait de l’objet auquel `a` fait référence.

## Définition des méthodes 

> Une méthode est une fonction définie dans une classe.

```py
class Point:
    def deplace(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy
```

Cette classe possède une méthode : `deplace()`.

Pour définir une méthode, il faut :
1.  indiquer son nom (ici `deplace()`).
2.  indiquer les arguments entre des parenthèses. Le premier argument d’une méthode doit toujours être `self` (c'est une référence à l'objet courant).
    

Pour accéder aux méthodes d’un objet, on indique :
1.  le nom de la variable qui fait référence à cet objet
2.  un point **`.`**
3.  le nom de la méthode

```py
a.deplace(3, 5)
```

> **Avertissement :**
>
> Lors de l’appel de la méthode, le paramètre `self` n’est pas utilisé et la valeur qu’il prend est la référence à l’objet. Il y a donc toujours un paramètre de moins que lors de la définition de la méthode.

**Exemple**

```py
class Point:
    def deplace(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

a = Point()
a.x = 1
a.y = 2
print("a : x =", a.x, "y =", a.y)
a.deplace(3, 5)
print("a : x =", a.x, "y =", a.y)
```

Affichage après exécution :
```
a : x = 1 y = 2
a : x = 4 y = 7
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+deplace(self,+dx,+dy)%3A%0A++++++++self.x+%3D+self.x+%2B+dx%0A++++++++self.y+%3D+self.y+%2B+dy%0A%0Aa+%3D+Point()%0Aa.x+%3D+1%0Aa.y+%3D+2%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aa.deplace(3,+5)%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

## La notion de constructeur 

Si lors de la création d’un objet nous voulons qu’un certain nombre d’actions soit réalisées (par exemple une initialisation), nous pouvons utiliser un **constructeur**.

Un constructeur n’est rien d’autre qu’une méthode, sans valeur de retour, qui porte un nom imposé par le langage Python : `__init__()`. Ce nom est constitué de `init` entouré avant et après par `__` (**deux fois** le symbole **underscore** `_`). Cette méthode sera appelée lors de la création de l’objet. Le constructeur peut disposer d’un nombre quelconque de paramètres, éventuellement aucun.

**Exemple sans paramètre**

```py
class Point:
    def __init__(self):
        self.x = 0
        self.y = 0

a = Point()
print("a : x =", a.x, "y =", a.y)
a.x = 1
a.y = 2
print("a : x =", a.x, "y =", a.y)
```

Affichage après exécution :
```
a : x = 0 y = 0
a : x = 1 y = 2
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+__init__(self)%3A%0A++++++++self.x+%3D+0%0A++++++++self.y+%3D+0%0A%0Aa+%3D+Point()%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aa.x+%3D+1%0Aa.y+%3D+2%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

> **IMPORTANT :**
> 
> Dans cet exemple, nous avons pu **définir des valeurs par défaut pour les attributs** grâce au constructeur.

**Exemple avec paramètres**

```py
class Point:
    def __init__(self, abs, ord):
        self.x = abs
        self.y = ord

a = Point(1, 2)
print("a : x =", a.x, "y =", a.y)
```

Affichage après exécution :
```
a : x = 1 y = 2
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+__init__(self,+abs,+ord)%3A%0A++++++++self.x+%3D+abs%0A++++++++self.y+%3D+ord%0A%0Aa+%3D+Point(1,+2)%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

> **ATTENTION :**
> Si le constructeur demande 2 paramètres, il faut absolument fournir 2 arguments lors de la création de l'objet.


**Autre exemple avec paramètres**

Dans l’exemple suivant, on utilise les mêmes noms pour les paramètres du constructeur et les attributs. Ceci ne pose pas de problème car ces variables ne sont pas dans le même espace de noms. Les paramètres du constructeur sont des variables locales, comme c’est habituellement le cas pour une fonction. Les attributs de l’objet sont eux dans l’espace de noms de l’instance. Les attributs se distinguent facilement car ils ont `self` devant.

```py
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

a = Point(1, 2)
print("a : x =", a.x, "y =", a.y)
```

[Exécuter avec Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+__init__(self,+x,+y)%3A%0A++++++++self.x+%3D+x%0A++++++++self.y+%3D+y%0A%0Aa+%3D+Point(1,+2)%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)



**Exemple complet**

```py
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def deplace(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

a = Point(1, 2)
b = Point(3, 4)
print("a : x =", a.x, "y =", a.y)
print("b : x =", b.x, "y =", b.y)
a.deplace(3, 5)
b.deplace(-1, -2)
print("a : x =", a.x, "y =", a.y)
print("b : x =", b.x, "y =", b.y)
```

Affichage après exécution :
```
a : x = 1 y = 2
b : x = 3 y = 4
a : x = 4 y = 7
b : x = 2 y = 2
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+__init__(self,+x,+y)%3A%0A++++++++self.x+%3D+x%0A++++++++self.y+%3D+y%0A%0A++++def+deplace(self,+dx,+dy)%3A%0A++++++++self.x+%3D+self.x+%2B+dx%0A++++++++self.y+%3D+self.y+%2B+dy%0A%0Aa+%3D+Point(1,+2)%0Ab+%3D+Point(3,+4)%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aprint(%22b+%3A+x+%3D%22,+b.x,+%22y+%3D%22,+b.y)%0Aa.deplace(3,+5)%0Ab.deplace(-1,+-2)%0Aprint(%22a+%3A+x+%3D%22,+a.x,+%22y+%3D%22,+a.y)%0Aprint(%22b+%3A+x+%3D%22,+b.x,+%22y+%3D%22,+b.y)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

**Exercice**

Modifier le programme de façon à ajouter deux autres objets de type `Point`. Ils seront référencés par des variables `c` et `d`.

**Exercice**

Définir une classe `Point3D` qui sera analogue à la classe `Point` mais pour des points dans l’espace à 3 dimensions. Créer deux objets de type `Point3D` qui seront référencés par les variables `a3D` et `b3D`. Initialiser ces points et afficher leurs coordonnées _x_, _y_, _z_.

# La notion d’encapsulation 

Le concept d”**encapsulation** est un concept très utile de la POO. Il permet en particulier d’éviter une modification par erreur des données d’un objet (les valeurs des attributs). En effet, il n’est alors pas possible d’agir directement sur les données d’un objet ; il est nécessaire de passer par ses méthodes qui jouent le rôle d’interface obligatoire.

## Définition d’attributs privés 

On réalise la protection des attributs de notre classe `Point` grâce à l’utilisation d’attributs privés. Pour avoir des attributs privés, leur nom doit débuter par `__` (**deux fois** le symbole **underscore** `_`, qui est le tiret sur la touche `8`).

```py
class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
```

Il n’est alors plus possible de faire appel aux attributs `__x` et `__y` depuis l’extérieur de la classe `Point`.

```py
>>> p = Point(1, 2)
>>> p.__x
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'Point' object has no attribute '__x'
```

Il faut donc disposer de méthodes qui vont permettre par exemple de modifier ou d’afficher les informations associées à ces variables.

```py
class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    def deplace(self, dx, dy):
        self.__x = self.__x + dx
        self.__y = self.__y + dy

    def affiche(self):
        print("abscisse =", self.__x, "ordonnee =", self.__y)

a = Point(2, 4)
a.affiche()
a.deplace(1, 3)
a.affiche()
```

[Exécuter](http://pythontutor.com/visualize.html#code=class+Point%3A%0A++++def+__init__(self,+x,+y)%3A%0A++++++++self.__x+%3D+x%0A++++++++self.__y+%3D+y%0A%0A++++def+deplace(self,+dx,+dy)%3A%0A++++++++self.__x+%3D+self.__x+%2B+dx%0A++++++++self.__y+%3D+self.__y+%2B+dy%0A%0A++++def+affiche(self)%3A%0A++++++++print(%22abscisse+%3D%22,+self.__x,+%22ordonnee+%3D%22,+self.__y)%0A%0Aa+%3D+Point(2,+4)%0Aa.affiche()%0Aa.deplace(1,+3)%0Aa.affiche()&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

## Accesseurs et mutateurs 

Parmi les différentes méthodes que comporte une classe, on a souvent tendance à distinguer :

*   les **constructeurs** ;
*   les **accesseurs** (en anglais _accessor_) qui fournissent des informations relatives à l’état d’un objet, c’est-à-dire aux valeurs de certains de ses attributs (généralement privés) sans les modifier ;
*   les **mutateurs** (en anglais _mutator_) qui modifient l’état d’un objet, donc les valeurs de certains de ses attributs.

On rencontre souvent l’utilisation de noms de la forme `get_XXXX()` pour les accesseurs et `set_XXXX()` pour les mutateurs, y compris dans des programmes dans lesquels les noms de variable sont francisés. Par exemple, pour la classe `Point` sur laquelle nous avons déjà travaillé on peut définir les méthodes suivantes :

**Exemple :**

```py
class Point:
    def __init__(self, x, y):
        self.set_x(x)
        self.set_y(y)

    def get_x(self):
        return self.__x

    def set_x(self, x):
        self.__x = x

    def get_y(self):
        return self.__y

    def set_y(self, y):
        self.__y = y

a = Point(3, 7)
print("a : abscisse =", a.get_x())
print("a : ordonnee =", a.get_y())
a.set_x(6)
a.set_y(10)
print("a : abscisse =", a.get_x())
print("a : ordonnee =", a.get_y())
```

[Exécuter dans Python Tutor](https://pythontutor.com/visualize.html#code=class%20Point%3A%0A%20%20%20%20def%20__init__%28self,%20x,%20y%29%3A%0A%20%20%20%20%20%20%20%20self.set_x%28x%29%0A%20%20%20%20%20%20%20%20self.set_y%28y%29%0A%20%20%20%20%0A%20%20%20%20def%20get_x%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self.__x%0A%0A%20%20%20%20def%20set_x%28self,%20x%29%3A%0A%20%20%20%20%20%20%20%20self.__x%20%3D%20x%0A%0A%20%20%20%20def%20get_y%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self.__y%0A%0A%20%20%20%20def%20set_y%28self,%20y%29%3A%0A%20%20%20%20%20%20%20%20self.__y%20%3D%20y%20%20%20%20%0A%20%20%20%20%20%20%20%20%0Aa%20%3D%20Point%283,%207%29%0Aprint%28%22a%20%3A%20abscisse%20%3D%22,%20a.get_x%28%29%29%0Aprint%28%22a%20%3A%20ordonnee%20%3D%22,%20a.get_y%28%29%29%0Aa.set_x%286%29%0Aa.set_y%2810%29%0Aprint%28%22a%20%3A%20abscisse%20%3D%22,%20a.get_x%28%29%29%0Aprint%28%22a%20%3A%20ordonnee%20%3D%22,%20a.get_y%28%29%29&cumulative=false&curInstr=0&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

L’utilisation d’un mutateur pour fixer la valeur d’un attribut autorise la possibilité d’effectuer un contrôle sur les valeurs de l’attribut. Par exemple, il serait possible de n’autoriser que des valeurs positives pour les attributs privés \_\_x et \_\_y.

Notez qu’il n’est pas toujours prudent de prévoir une méthode d’accès pour chacun des attributs privés d’un objet. En effet, il ne faut pas oublier qu’il doit toujours être possible de modifier l’implémentation d’une classe de manière transparente pour son utilisateur.

**Exemple avec le décorateur @property**

Il existe en Python une autre approche pour gérer ce type de situation. Elle utilise le décorateur `@property`.

```py
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

a = Point(3, 7)
print("a : abscisse =", a.x)
print("a : ordonnee =", a.y)
a.x = 6
a.y = 10
print("a : abscisse =", a.x)
print("a : ordonnee =", a.y)
```

[Exécuter dans Python Tutor](https://pythontutor.com/visualize.html#code=class%20Point%3A%0A%20%20%20%20def%20__init__%28self,%20x,%20y%29%3A%0A%20%20%20%20%20%20%20%20self.x%20%3D%20x%0A%20%20%20%20%20%20%20%20self.y%20%3D%20y%0A%0A%20%20%20%20%40property%0A%20%20%20%20def%20x%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self._x%0A%0A%20%20%20%20%40x.setter%0A%20%20%20%20def%20x%28self,%20x%29%3A%0A%20%20%20%20%20%20%20%20self._x%20%3D%20x%0A%0A%20%20%20%20%40property%0A%20%20%20%20def%20y%28self%29%3A%0A%20%20%20%20%20%20%20%20return%20self._y%0A%0A%20%20%20%20%40y.setter%0A%20%20%20%20def%20y%28self,%20y%29%3A%0A%20%20%20%20%20%20%20%20self._y%20%3D%20y%0A%0Aa%20%3D%20Point%283,%207%29%0Aprint%28%22a%20%3A%20abscisse%20%3D%22,%20a.x%29%0Aprint%28%22a%20%3A%20ordonnee%20%3D%22,%20a.y%29%0Aa.x%20%3D%206%0Aa.y%20%3D%2010%0Aprint%28%22a%20%3A%20abscisse%20%3D%22,%20a.x%29%0Aprint%28%22a%20%3A%20ordonnee%20%3D%22,%20a.y%29&cumulative=false&curInstr=0&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

On utilise des attributs précédés par un underscore. Il s’agit d’une convention pour indiquer que ces attributs ne doivent pas être utilisés en dehors de la classe et qu’il faut utiliser les accesseurs et mutateurs pour les manipuler.

**Exercice**

Définir une classe `Point3D` analogue à la classe `Point` mais pour des points dans l’espace à 3 dimensions.

# Attributs et méthodes **de classe** 
Jusqu'à maintenant, les attributs et méthodes dont nous avons parlé sont des attributs d'instance (d'objet) et des méthodes d'instance.
La valeur d'un attribut d'instance est stockée dans l'instance.

**Les attributs de classe et les méthodes de classe sont stockés dans la classe et sont partagés avec toutes les instances de la classe**.

## Attributs de classe 

**Exemple :**

```py
class A:
    nb = 0

    def __init__(self, x):
        print("creation objet de type A")
        self.x = x
        A.nb = A.nb + 1

print("A : nb = ", A.nb)
print("Partie 1")
a = A(3)
print("A : nb = ", A.nb)
print("a : x = ", a.x, " nb = ", a.nb)
print("Partie 2")
b = A(6)
print("A : nb = ", A.nb)
print("a : x = ", a.x, " nb = ", a.nb)
print("b : x = ", b.x, " nb = ", b.nb)
c = A(8)
print("Partie 3")
print("A : nb = ", A.nb)
print("a : x = ", a.x, " nb = ", a.nb)
print("b : x = ", b.x, " nb = ", b.nb)
print("c : x = ", c.x, " nb = ", c.nb)
```
Résultat après exécution :
```
A : nb =  0
Partie 1
creation objet de type A
A : nb =  1
a : x =  3  nb =  1
Partie 2
creation objet de type A
A : nb =  2
a : x =  3  nb =  2
b : x =  6  nb =  2
creation objet de type A
Partie 3
A : nb =  3
a : x =  3  nb =  3
b : x =  6  nb =  3
c : x =  8  nb =  3
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+A%3A%0D%0A++++nb+%3D+0%0D%0A%0D%0A++++def+__init__(self,+val)%3A%0D%0A++++++++print(%22creation+objet+de+type+A%22)%0D%0A++++++++self.x+%3D+val%0D%0A++++++++A.nb+%3D+A.nb+%2B+1%0D%0A%0D%0Aprint(%22A+%3A+nb+%3D+%22,+A.nb)%0D%0Aprint(%22Partie+1%22)%0D%0Aa+%3D+A(3)%0D%0Aprint(%22A+%3A+nb+%3D+%22,+A.nb)%0D%0Aprint(%22a+%3A+x+%3D+%22,+a.x,+%22+nb+%3D+%22,+a.nb)%0D%0Aprint(%22Partie+2%22)%0D%0Ab+%3D+A(6)%0D%0Aprint(%22A+%3A+nb+%3D+%22,+A.nb)%0D%0Aprint(%22a+%3A+x+%3D+%22,+a.x,+%22+nb+%3D+%22,+a.nb)%0D%0Aprint(%22b+%3A+x+%3D+%22,+b.x,+%22+nb+%3D+%22,+b.nb)%0D%0Ac+%3D+A(8)%0D%0Aprint(%22Partie+3%22)%0D%0Aprint(%22A+%3A+nb+%3D+%22,+A.nb)%0D%0Aprint(%22a+%3A+x+%3D+%22,+a.x,+%22+nb+%3D+%22,+a.nb)%0D%0Aprint(%22b+%3A+x+%3D+%22,+b.x,+%22+nb+%3D+%22,+b.nb)%0D%0Aprint(%22c+%3A+x+%3D+%22,+c.x,+%22+nb+%3D+%22,+c.nb)&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

## Méthodes de classe 

**Exemple :**

```py
class A:
    nb = 0

    def __init__(self):
        print("creation objet de type A")
        A.nb = A.nb + 1
        print("il y en a maintenant ", A.nb)

    @classmethod
    def get_nb(cls):
        return A.nb

print("Partie 1 : nb objets = ", A.get_nb())
a = A()
print("Partie 2 : nb objets = ", A.get_nb())
b = A()
print("Partie 3 : nb objets = ", A.get_nb())
```

[Exécuter dans Python Tutor](http://pythontutor.com/visualize.html#code=class+A%3A%0A++++nb+%3D+0%0A%0A++++def+__init__(self)%3A%0A++++++++print(%22creation+objet+de+type+A%22)%0A++++++++A.nb+%3D+A.nb+%2B+1%0A++++++++print(%22il+y+en+a+maintenant+%22,+A.nb)%0A%0A++++%40classmethod%0A++++def+get_nb(cls)%3A%0A++++++++return+A.nb%0A%0Aprint(%22Partie+1+%3A+nb+objets+%3D+%22,+A.get_nb())%0Aa+%3D+A()%0Aprint(%22Partie+2+%3A+nb+objets+%3D+%22,+A.get_nb())%0Ab+%3D+A()%0Aprint(%22Partie+3+%3A+nb+objets+%3D+%22,+A.get_nb())&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&rawInputLstJSON=%5B%5D&curInstr=0)

Pour créer une méthode de classe, il faut la faire précéder d’un « décorateur » : `@classmethod`

Le premier argument de la méthode de classe doit être `cls`.

Voir aussi : [https://python.developpez.com/cours/apprendre-python3/?page=page\_13#L13](https://python.developpez.com/cours/apprendre-python3/?page=page_13#L13)