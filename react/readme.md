# Commencer en React

> * Auteur : Gwénaël LAURENT
> * Date : 24/04/2020
> * OS : Windows 10 (version 1903)
> * VScode : version 1.43.0 (system setup)
> * Node.js : version v12.16.2
> * JDK : version 1.8.0_192

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

- [Commencer en React](#commencer-en-react)
- [React vs React native](#react-vs-react-native)
- [Pré-requis](#pr%c3%a9-requis)
- [Tutoriels React](#tutoriels-react)
- [Tutoriels React native](#tutoriels-react-native)
- [VScode pour coder du React](#vscode-pour-coder-du-react)

# React vs React native
* **React** est un framework Web Front qui permet de créer des interfaces web
* **React native** est un framework mobile qui utilise React pour construire des applis mobiles (android ou iOS)

# Pré-requis
* Codage Javascript
  * [Une réintroduction à JavaScript (mozilla.org)](https://developer.mozilla.org/fr/docs/Web/JavaScript/Une_r%C3%A9introduction_%C3%A0_JavaScript)
* ES6 : import, let, const et fonctions fléchées
  * [Learn ES2015 (Babeljs)](https://babeljs.io/docs/en/learn/)
* promise :
  * [Vidéo - Apprendre le JavaScript : Chapitre 21, Promise, Async & Await (à partir de 8:50)](https://www.youtube.com/watch?v=uUZxHkcidps&t=530)
* Node.js
  * [Node Hero - Getting Started With Node.js Tutorial](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/)
* installation de l'environnement de dév sur votre poste ou dans des sites en ligne

# Tutoriels React
* [Tutoriel : intro à React (fr.reactjs.org)](https://fr.reactjs.org/tutorial/tutorial.html)
* [Guide pas à pas : Bonjour monde ! (fr.reactjs.org)](https://fr.reactjs.org/docs/hello-world.html)
* [Introduction à JSX (fr.reactjs.org)](https://fr.reactjs.org/docs/introducing-jsx.html)
* [Vidéo - REACT : 1H POUR COMPRENDRE LA LIBRAIRIE !](https://www.youtube.com/watch?v=no82oluCZag)

# Tutoriels React native
* [Getting Started (facebook.github.io)](https://facebook.github.io/react-native/docs/getting-started)
* [Learn the Basics (facebook.github.io)](https://facebook.github.io/react-native/docs/tutorial.html)
* [Vidéo - Débuter avec React Native](https://www.youtube.com/watch?v=iTmwNQ4WSjY)
* [Vidéo - Débuter avec React Native (ancienne version)](https://www.youtube.com/watch?v=Gp1tKdonWMI)

# VScode pour coder du React
* [Using React in Visual Studio Code](https://code.visualstudio.com/docs/nodejs/reactjs-tutorial)
* [CONFIGURER SON VISUAL STUDIO CODE POUR REACT](http://blog.ai3.fr/configurer-son-visual-studio-code-pour-react/)
* [Extension React Native Tools](https://marketplace.visualstudio.com/items?itemName=msjsdiag.vscode-react-native)
* [Configuration de VS Code pour le développement React Native](https://www.dwastudio.fr/article/configuration-de-vs-code-pour-le-developpement-react-native/)
