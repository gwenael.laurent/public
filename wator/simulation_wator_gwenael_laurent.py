# -*- coding:utf-8 -*-

"""
:mod: WA-TOR simulation proie-pédateur
:author: Gwénaël LAURENT
:date: 27/05/2019
"""

import random
#import matplotlib

"""
Variables globales
"""
#caractéristiques initiales des poissons
duree_gestation_thons = 2.0
energie_init_requins = 3.0
duree_gestation_requins = 6.0

# enregister à chaque pas de simulation le triplet
# (numéro du pas, nombre de thons, nombre de requins)
resultat_simul = []

"""
Constantes globales
"""
THON = 'thon'
REQUIN = 'requin'
VIDE = None


def creerMer(nL, nC):
    """
    Création de la grille de mer remplie avec None (ni thon, ni requin)
    :param nL: (int) nb de lignes
    :param nC: (int) nb de colonnes
    :return: (liste de listes) = grille de la simulation 

    :CU: nL > 0 et nC > 0
    :effet de bord: aucun

    >>> creerMer(2, 3)
    [[None, None, None], [None, None, None]]

    """

    gr = []
    for i in range(nL):
        uneLigne = [VIDE]*nC
        gr.append(uneLigne)
    return gr


def caractPoisson(typeP):
    """
    Création de la structure de données (dictionnaire) d'un poisson
    avec ses caractéristiques initiales
    :param typeP: (string) THON ou REQUIN
    :return: (dict) un thon ou un poisson

    :CU: typeP = 'thon' ou 'requin'
    :effet de bord: aucun

    >>> type(caractPoisson(REQUIN))
    <class 'dict'>
    >>> caractPoisson(THON)
    {'type': 'thon', 'gestation': 2.0}
    >>> caractPoisson(REQUIN)
    {'type': 'requin', 'gestation': 6.0, 'energie': 3.0}

    """
    if typeP == THON:
        return {'type': THON,
                'gestation': duree_gestation_thons }
    elif typeP == REQUIN:
        return {'type': REQUIN,
                'gestation': duree_gestation_requins,
                'energie': energie_init_requins  }


def creerPoisson(gr, caseSelect, typeP):
    """
    Création d'un poisson dans une case de la mer. Le poisson est créé
    avec ses caractéristiques initiales.
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case
    :param typeP: (string) THON ou REQUIN
    :return: (NoneType)

    :CU: typeP = 'thon' ou 'requin'
         caseSelect fait partie de gr
    :effet de bord: aucun

    >>> mer = creerMer(2, 3)
    >>> creerPoisson(mer, (0,1), THON)
    >>> creerPoisson(mer, (1,0), REQUIN)
    >>> mer[0][1]
    {'type': 'thon', 'gestation': 2.0}

    """
    gr[caseSelect[0]][caseSelect[1]] = caractPoisson(typeP)


def remplirMer(gr, pctThon, pctRequin):
    """
    Rempli la grille aléatoirement avec des thons et des requins.
    :param gr: (liste de listes) une grille de la mer
    :param pctThon: (int) pourcentage de thon
    :param pctRequin: (int) pourcentage de requin
    :return: (liste de listes) la grille de la mer remplie

    :CU: pctThon + pctRequin <= 100
    :effet de bord: modification des données de gr

    >>> mer = remplirMer(creerMer(2,5), 20, 30)
    >>> mer
    [...]
    >>> len(mer)
    2
    >>> len(mer[0])
    5

    """
    # Créer une liste locale de taille = nC*nL remplie de 0
    nbCasesMer = len(gr) * len(gr[0])
    listTemp = [0]*nbCasesMer
    # Remplir cette liste avec les pourcentages de thon (1) et de requin (2)
    nbThon = int(nbCasesMer * (pctThon / 100))
    nbRequin = int(nbCasesMer * (pctRequin / 100))
    listTemp[0:nbThon] = [1]*nbThon
    listTemp[nbThon:nbRequin+nbThon] = [2]*nbRequin
    
    # Mélanger cette liste
    random.shuffle(listTemp)
    
    # Suivre l'ordre de cette liste pour remplir la mer
    nbC = len(gr[0])
    for k, val in enumerate(listTemp):
        ligne = k // nbC
        colonne = k % nbC
        if val == 1:
            creerPoisson(gr, (ligne, colonne), THON)
        elif val == 2:
            creerPoisson(gr, (ligne, colonne), REQUIN)
    return gr


def nombrePoissons(gr, typeP):
    """
    Compte le nombre de poissons pour le type demandé.
    :param gr: (liste de listes) une grille de la mer
    :param typeP: (string) THON ou REQUIN
    :return: (int) le nombre de poisson du typeP

    :CU: typeP = THON ou REQUIN
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(2,5), 20, 30)
    >>> nombrePoissons(mer, THON)
    2
    >>> nombrePoissons(mer, REQUIN)
    3
    """
    nbP = 0
    for l in gr:
        for c in l:
            if c != VIDE:
                if c['type'] == typeP :
                    nbP += 1
    return nbP


def selectionCase(gr):
    """
    Sélectionne aléatoirement une case de la grille
    :param gr: (liste de listes) une grille de la mer
    :return: (tuple) les coordonnées (ligne, colonne) de la case sélectionnée

    :CU: aucun
    :effet de bord: aucun

    >>> type(selectionCase(creerMer(2,5)))
    <class 'tuple'>

    """
    nbCase = len(gr) * len(gr[0])
    c = random.randrange(nbCase)
    
    ligne = c // len(gr[0])
    colonne = c % len(gr[0])
    return (ligne, colonne)


def typeOccupationCase(gr, caseSelect):
    """
    Renvoie le contenu de la case sélectionnée : None (vide), THON ou REQUIN
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case
    :return: None, (string) THON ou (string) REQUIN

    :CU: aucun
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(2,5), 20, 30)
    >>> caseSelect = selectionCase(mer)
    >>> typeOccupationCase(mer, caseSelect) in (None, THON, REQUIN)
    True

    """
    case = gr[caseSelect[0]][caseSelect[1]]
    if case == VIDE :
        return VIDE
    elif case['type'] == THON :
        return THON
    elif case['type'] == REQUIN :
        return REQUIN


def occupantCase(gr, caseSelect):
    """
    Renvoie le contenu de la case sélectionnée : None, un dictionnaire de thon
    ou un dictionnaire de requin
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case
    :return: None, un dictionnaire de thon (dict) ou un dictionnaire de requin (dict)

    :CU: aucun
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(2,5), 20, 30)
    >>> case = selectionCase(mer)
    >>> type(occupantCase(mer, case)) == (dict) or occupantCase(mer, case) == None
    True

    """
    return gr[caseSelect[0]][caseSelect[1]]


def casesVoisines(gr, caseSelect):
    """
    Trouver les 4 cases voisines dans une mer torique (ordre aléatoire).
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case sélectionnée
    :return: (liste de tuples) (ligne, colonne) pour les coordonnées des 4 cases voisines

    :CU: aucun
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(4,5), 20, 30)
    >>> len(casesVoisines(mer, (2,2)))
    4
    >>> type(casesVoisines(mer, (3,1))[0])
    <class 'tuple'>

    """
    lSelect = caseSelect[0]  # ligne de la case sélectionnée
    cSelect = caseSelect[1]  # colonne de la case sélectionnée
    nbL = len(gr)  # nombre de ligne de la mer
    nbC = len(gr[0])  # nombre de colonne de la mer

    # case voisine N (même colonne)
    lN = (lSelect - 1) % nbL
    cN = cSelect

    # case voisine E (même ligne)
    lE = lSelect
    cE = (cSelect + 1) % nbC

    # case voisine S (même colonne)
    lS = (lSelect + 1) % nbL
    cS = cSelect

    # case voisine O (même ligne)
    lO = lSelect
    cO = (cSelect - 1) % nbC

    valR = [(lN, cN), (lE, cE), (lS, cS), (lO, cO)]
    random.shuffle(valR)
    return valR


def trouverCaseLibre(gr, cases_voisines):
    """
    Trouver une case libre parmi les cases voisines.
    Comme la fonction `casesVoisines()` renvoie la liste des cases voisines
    en ordre aléatoire, il suffit de trouver la première case libre parmi
    les cases voisines.
    :param gr: (liste de listes) une grille de la mer
    :param cases_voisines: (liste de tuples) (ligne, colonne) pour les coordonnées
                            des 4 cases voisines
    :return: (tuple) les coordonnées d'une case libre
             (int) -1 s'il n'y a pas de case libre

    :CU: cases_voisines est une liste de tuples
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(4,5), 20, 30)
    >>> type(trouverCaseLibre(mer, casesVoisines(mer, (0,0)))) in (int, tuple)
    True

    """
    for case in cases_voisines:
        if typeOccupationCase(gr, case) == VIDE:
            return case
    return -1


def trouverThon(gr, cases_voisines):
    """
    Trouver une case occupée par un thon parmi les cases voisines.
    Comme la fonction `casesVoisines()` renvoie la liste des cases voisines
    en ordre aléatoire, il suffit de trouver la première case occupée par
    un thon parmi les cases voisines.
    :param gr: (liste de listes) une grille de la mer
    :param cases_voisines: (liste de tuples) (ligne, colonne) pour les coordonnées
                            des 4 cases voisines
    :return: (tuple) les coordonnées d'une case occupée par un thon
             (int) -1 s'il n'y a pas de case occupée par un thon

    :CU: cases_voisines est une liste de tuples
    :effet de bord: aucun

    >>> mer = remplirMer(creerMer(4,5), 20, 30)
    >>> type(trouverThon(mer, casesVoisines(mer, (3,4)))) in (int, tuple)
    True

    """
    for case in cases_voisines:
        if typeOccupationCase(gr, case) == THON:
            return case
    return -1


def deplacerPoisson(gr, ancienneCase, nvlleCase):
    """
    Déplacer le poisson de l'ancienne case à la nouvelle.
    L'ancienne case devient une case vide de la mer (= None)
    :param gr: (liste de listes) une grille de la mer
    :param ancienneCase: (tuple) coordonnées de l'ancienne case (départ)
    :param nvlleCase: (tuple) coordonnées de la nouvelle case (arrivée)
    :return: (NoneType) 

    >>> mer = remplirMer(creerMer(4,5), 20, 30)
    >>> deplacerPoisson(mer, (0,0), (0,4))
    >>> type(occupantCase(mer, (0,0)))
    <class 'NoneType'>
    
    :CU: ancienneCase != nvlleCase
    :effet de bord: modification du contenu de gr

    """
    gr[nvlleCase[0]][nvlleCase[1]] = gr[ancienneCase[0]][ancienneCase[1]]
    gr[ancienneCase[0]][ancienneCase[1]] = VIDE


def diminuerGestation(gr, caseSelect):
    """
    Diminue de 1 le temps de gestation d'un poisson et renvoie la nouvelle valeur
    de la gestation.
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case sélectionnée
    :return: (int) la nouvelle valeur de la gestation
             (-1) si caseSelect ne contient pas de poisson

    :CU: caseSelect contient un THON ou un REQUIN
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(4, 5)
    >>> creerPoisson(mer, (3,2), THON)
    >>> type(diminuerGestation(mer, (3,2)))
    <class 'float'>

    """

    gr[caseSelect[0]][caseSelect[1]]['gestation'] -= 1.0
    return gr[caseSelect[0]][caseSelect[1]]['gestation']


def reinitialiserGestation(gr, caseSelect, typeP):
    """
    Réinitialise le temps de gestation d'un poisson à sa valeur initiale
    et renvoie la nouvelle valeur de la gestation.
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case sélectionnée
    :param typeP: (string) THON ou REQUIN
    :return: (int) la nouvelle valeur de la gestation

    :CU: caseSelect contient un THON ou un REQUIN
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(4, 5)
    >>> creerPoisson(mer, (1,4), REQUIN)
    >>> type(reinitialiserGestation(mer, (1,4), REQUIN))
    <class 'float'>

    """

    if typeP == THON:
        gr[caseSelect[0]][caseSelect[1]]['gestation'] = duree_gestation_thons 
        return gr[caseSelect[0]][caseSelect[1]]['gestation']
    elif typeP == REQUIN:
        gr[caseSelect[0]][caseSelect[1]]['gestation'] = duree_gestation_requins 
        return gr[caseSelect[0]][caseSelect[1]]['gestation']


def diminuerEnergie(gr, caseSelect):
    """
    Diminue de 1 le niveau d'énergie d'un requin et renvoie la nouvelle
    valeur d'énergie.
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case sélectionnée
    :return: (int) le nouveau niveau d'énergie

    :CU: caseSelect contient un REQUIN
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(4, 5)
    >>> creerPoisson(mer, (0,1), REQUIN)
    >>> type(diminuerEnergie(mer, (0,1)))
    <class 'float'>

    """

    gr[caseSelect[0]][caseSelect[1]]['energie'] -= 1.0
    return gr[caseSelect[0]][caseSelect[1]]['energie']



def reinitialiserEnergie(gr, caseSelect):
    """
    Réinitialise le niveau d'énergie d'un requin à sa valeur initiale
    et renvoie la nouvelle valeur d'énergie.
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées (ligne, colonne) de la case sélectionnée
    :return: (int) la nouvelle valeur d'énergie

    :CU: caseSelect contient un REQUIN
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(4, 5)
    >>> creerPoisson(mer, (3,1), REQUIN)
    >>> type(reinitialiserEnergie(mer, (3,1)))
    <class 'float'>

    """

    gr[caseSelect[0]][caseSelect[1]]['energie'] = energie_init_requins 
    return gr[caseSelect[0]][caseSelect[1]]['energie']


def comportementThon(gr, caseSelect):
    """
    Met en oeuvre le comportement complet d'un thon
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées d'une case contenant un thon
    :return: (NoneType)

    :CU: caseSelect contient un THON
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(2, 3)
    >>> creerPoisson(mer, (0,1), THON)
    >>> comportementThon(mer, (0,1))

    """

    # -------------------------
    #  Déplacement
    # -------------------------

    # enregistrement de la place du thon
    caseThon = caseSelect
    # Le thon choisit aléatoirement une case libre parmi ses voisines
    caseLibre = trouverCaseLibre(gr, casesVoisines(gr, caseSelect))
    # S'il en existe une, le thon se déplace vers cette case
    if caseLibre != -1:
        deplacerPoisson(gr, caseSelect, caseLibre)
        caseThon = caseLibre

    # -------------------------
    #  Reproduction
    # -------------------------

    # Le temps de gestation du thon est diminué de 1
    nouveauTempsGest = diminuerGestation(gr, caseThon)
    # Si ce temps arrive à 0,
    if nouveauTempsGest <= 0:
        # si le thon s'est déplacé
        if caseLibre != -1:
            # le thon donne naissance à un nouveau thon qui naît
            # sur la case qu'il vient de quitter
            creerPoisson(gr, caseSelect, THON)
        # Le temps de gestation est remis à sa valeur initiale
        reinitialiserGestation(gr, caseThon, THON)


def comportementRequin(gr, caseSelect):
    """
    Met en oeuvre le comportement complet d'un requin
    :param gr: (liste de listes) une grille de la mer
    :param caseSelect: (tuple) coordonnées d'une case contenant un requin
    :return: (NoneType)

    :CU: caseSelect contient un REQUIN
    :effet de bord: modifie le contenue de mer

    >>> mer = creerMer(2, 3)
    >>> creerPoisson(mer, (0,1), THON)
    >>> creerPoisson(mer, (1,1), REQUIN)
    >>> comportementRequin(mer, (1,1))

    """

    # -------------------------
    #  Energie
    # -------------------------

    # Le requin perd un point d'énergie
    energie = diminuerEnergie(gr, caseSelect)

    # -------------------------
    #  Déplacement
    # -------------------------

    # enregistrement de la place du requin
    caseRequin = caseSelect
    # Le requin choisit aléatoirement parmi ses voisines
    # une case occupée par un thon
    caseThon = trouverThon(gr, casesVoisines(gr, caseSelect))
    # S'il en existe une
    if caseThon != -1:
        # le requin se déplace vers cette case et mange le thon
        deplacerPoisson(gr, caseSelect, caseThon)
        # enregistrement de la nouvelle place du requin
        caseRequin = caseThon
        # Son niveau d'énergie est alors remis à sa valeur initiale
        reinitialiserEnergie(gr, caseRequin)
    # Seulement s'il n'existe pas de case voisine occupée par un thon
    else:
        # il cherche à se déplacer vers une case voisine vide
        caseLibre = trouverCaseLibre(gr, casesVoisines(gr, caseSelect))
        # S'il existe une case libre,
        if caseLibre != -1:
            # le requin se déplace vers cette case
            deplacerPoisson(gr, caseSelect, caseLibre)
            # enregistrement de la nouvelle place du requin
            caseRequin = caseLibre

    # -------------------------
    #  Mort
    # -------------------------

    # Si le niveau d'énergie du requin est à 0
    if energie <= 0:
        # le requin meurt
        gr[caseRequin[0]][caseRequin[1]] = VIDE
    else:
        # -------------------------
        #  Reproduction
        # -------------------------

        # Le temps de gestation du requin est diminué de 1
        nouveauTempsGest = diminuerGestation(gr, caseRequin)
        # Si ce temps arrive à 0
        if nouveauTempsGest <= 0:
            # s'il s'est déplacé
            if caseRequin != caseSelect:
                # il donne naissance à un nouveau requin
                # sur la case qu'il vient de quitter
                creerPoisson(gr, caseSelect, REQUIN)
            # Son temps de gestation est remis à sa valeur initiale
            reinitialiserGestation(gr, caseRequin, REQUIN)


def afficherMerConsole(gr):
    """
    PROCEDURE pour afficher une grille de mer pendant la simulation
    Les cases vides seront affichées avec un tiret (-)
    Les cases contenant un THON seront affichées avec un o majuscule (O).
    Les cases contenant un REQUIN seront affichées avec un x majuscule (X).
    Le contenu des cases sera séparé par une espace.
    Chaque ligne de la grille sera affichée sur une ligne distincte.
    :param gr: (liste de listes) une grille de la mer
    :return (NoneType)

    """
    aff = ""
    for l in range(0, len(gr)):
        for c in range(0, len(gr[l])):
            if typeOccupationCase(gr, (l, c)) == THON:
                aff += "O "
            elif typeOccupationCase(gr, (l, c)) == REQUIN:
                aff += "X "
            else:
                aff += "- "
        aff += "\n"
    print(aff + "\n")


def simulationWaTor(nL, nC, pctThon, pctRequin, nbPas, listResults, afficher=False):
    """
    Simulation Wa-Tor
    :param nL: (int) nb de lignes
    :param nC: (int) nb de colonnes
    :param pctThon: (int) pourcentage de thon
    :param pctRequin: (int) pourcentage de requin
    :param nbPas: (int) nombre de pas de la simulation
    :param listResults: (liste de triplet) (numéro du pas, nombre de thons, nombre de requins)
    :param afficher: (bool) active l'affichage de la mer tous les 100 pas
    
    :CU: nL > 0 et nC > 0
         pctThon + pctRequin <= 100
    :effet de bord: modification de la liste globale resultat_simul
    """

    # Créer la mer et la remplir aléatoirement d'un pourcentage de thon et de requin.
    mer = remplirMer(creerMer(nL, nC), pctThon, pctRequin)

    # À chaque pas de la simulation
    i = 1
    while i <= nbPas:
        # une case de la mer est sélectionnée aléatoirement
        caseSelect = selectionCase(mer)

        # Si elle est occupée par un thon
        occupant = typeOccupationCase(mer, caseSelect)
        if occupant == THON:
            comportementThon(mer, caseSelect)
        # Si elle est occupée par un requin
        elif occupant == REQUIN:
            comportementRequin(mer, caseSelect)
        # Ajouter un tuple dans listResults
            # (numéro du pas, nombre de thons, nombre de requins)
        listResults.append((i, nombrePoissons(mer, THON),
                            nombrePoissons(mer, REQUIN)))
        # affichage de la mer tous les 100 pas
        if afficher == True:
            if i % 100 == 0:
                afficherMerConsole(mer)
        # incrément du pas
        i = i + 1


def afficherCourbes(listResults):
    """
    Affichage des courbes de l'évolution des populations
    de thons et de requins
    :param listResults: (liste de triplet) (numéro du pas, nombre de thons, nombre de requins)
    :return: (NoneType)

    :CU: affichage après simulation complète
    """
    import pylab
    data_pas = []  # une liste d'abscisses (nombre de pas)
    data_thons = []  # une liste d'ordonnées (nombre de thon)
    data_requins = []  # une liste d'ordonnées (nombre de requin)
    for r in listResults:
        data_pas.append(r[0])
        data_thons.append(r[1])
        data_requins.append(r[2])
    pylab.plot(data_pas, data_thons)
    pylab.plot(data_pas, data_requins)
    pylab.title('Evolution des populations de thons et de requins')
    pylab.show()


if __name__ == '__main__':
    import doctest
    doctest.testmod(
        verbose=False, optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS)
    # http://www.fil.univ-lille1.fr/~L1S2API/CoursTP/tp_doctest.html
    # https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc1/bonnes_pratiques/triangle_pascal.py


"""
Lancement de la simulation
"""
simulationWaTor(25, 25, 30, 10, 125000, resultat_simul, True)
afficherCourbes(resultat_simul)
