WA-TOR Simulation proie-prédateur
=================================
Auteurs :
=========
- Présentation de la simulation : fil.univ-lille.fr
- Modifications de la présentation : Gwénaël LAURENT
- Travail à réaliser : Gwénaël LAURENT
- _03  juin 2019_


Objectifs pédagogiques :
==========================
**Prérequis** : 
* types liste
* notion de tuple
* notion de dictionnaire
* module random
* opérateur modulo
* docstring et automatisation des doctest

**Objectifs** : 
* Utiliser les fonctions pour décomposer un problème.
* Les fonctions doivent toutes être testées individuellement
* Utiliser des dictionnaires pour associer des propriétés à une donnée (ici le requin, le thon et la config)
* Utiliser des tuples pour identifier les cases voisines (séquences non modifiables) et pour stocker les pas de la simulation
* Utiliser les docstring et doctest automatisés



Présentation de la simulation
=============================
**Simulation proie-prédateur** ([article wikipédia](https://en.wikipedia.org/wiki/Wa-Tor) (en anglais))

Wa-Tor est une simulation de type proie-prédateur. Dans une mer torique évoluent des thons (les proies) et des requins (les prédateurs). Les uns et les autres se déplacent et se reproduisent. Pour acquérir l'énergie suffisante à sa survie un requin doit manger un thon régulièrement. Un thon vit éternellement tant qu'il n'est pas mangé par un requin.

![Un thon](./images/thon.png) ![Un requin](./images/requin.jpg)

## La mer

La mer est représentée par une grille à deux dimensions.  Chaque case a quatre voisines, une dans chacune des quatre directions cardinales (N, E, S, O).
Chaque case de cette grille représente une zone de mer qui peut être soit vide, soit occupée par un thon ou un requin.

**Attention** : une case de la grille a 4 voisines, y compris pour les cases sur le bord de la grille.
Dans ce cas, _on considère que la grille est "repliée" comme un tore_. C'est à dire qu'une case en bas de la grille a pour voisine une case du haut de la grille, de même pour les bords gauche et droit.

Exemple de tore : Source https://fr.wikipedia.org/wiki/Tore
    
![Un tore](./images/tore.png)


## Les poissons

Chaque thon est caractérisé par son *temps de gestation*. Ce temps est initialisé à une valeur initiale commune à tous les thons, appelée *durée de gestation des thons*.

Chaque requin est caractérisé par son *temps de gestation* et son *énergie*. Ces deux valeurs sont initialisées à une valeur initiale commune à tous les requins, appelées respectivement *durée de gestation des requins* et *énergie des requins*.

## Simulation et comportements

À chaque pas de la simulation, une case de la mer est sélectionnée aléatoirement. Si elle est vide, il ne se passe rien. Si elle est occupée par un poisson, on applique alors son comportement. Les comportements des thons et des requins sont régis par des règles simples.

 * **Un thon** applique le comportement suivant :
   1. *Déplacement* Le thon choisit aléatoirement une case libre parmi ses voisines. S'il en existe une, le thon se déplace vers cette case. Il reste sur place sinon. 
   2. *Reproduction* 
     - Le temps de gestation du thon est diminué de 1. 
     - Si ce temps arrive à 0, le thon donne naissance à un nouveau thon qui naît sur la case qu'il vient de quitter s'il s'est déplacé. Sinon aucun thon ne naît. Le temps de gestation est remis à sa valeur initiale (même si le déplacement n'a pas été possible).

 * **Un requin** applique le comportement suivant :
   1. *Energie* Le requin perd un point d'énergie.
   2. *Déplacement* Le requin choisit aléatoirement parmi ses voisines une case occupée par un thon (les requins privilégient les déplacements vers les cases occupées par un thon). S'il en existe une, le requin se déplace vers cette case et mange le thon. Son niveau d'énergie est alors remis à sa valeur initiale. Seulement s'il n'existe pas de case voisine occupée par un thon, il cherche à se déplacer vers une case voisine vide choisie au hasard. Il reste sur place s'il n'y en a aucune.
   3. *Mort* Si le niveau d'énergie du requin est à 0, il meurt. Dans ce cas l'étape suivante n'a évidemment pas lieu.
   4. *Reproduction* 
    - Le temps de gestation du requin est diminué de 1. 
    - Si ce temps arrive à 0, il donne naissance à un nouveau requin sur la case qu'il vient de quitter s'il s'est déplacé, sinon aucun requin ne naît. Son temps de gestation est remis à sa valeur initiale.


## Phénomènes proies-prédateurs émergents

Les durées de gestation des deux espèces et l'énergie récupérée par un requin lorsqu'il mange un thon sont des paramètres de la simulation. S'ils sont bien choisis on peut voir émerger un phénomène "périodique" d'évolution des populations.

Quand il y a peu de prédateurs, la population des proies augmente, mais cette abondance de proies permet alors aux prédateurs de facilement trouver l'énergie suffisante pour leur survie et leur reproduction. Leur population se met alors à croître au détriment de celle de leur proie. Quand ces dernières deviennent trop rares, les prédateurs finissent par mourir sans se reproduire. Et quand il y a peu de prédateurs...

Pour obtenir ce phénomène cyclique,  il faut respecter les inégalités suivantes :

*temps gestation des thons < énergie des requins < durée gestation des requins*

Les valeurs suggérées sont 2, 3 et 6.

Il est également souhaitable de débuter avec une configuration dans laquelle il y a un nombre de thons relativement important par rapport au nombre de requins. Par exemple 30% des cases de la mer sont initialement occupées par des thons et 10% par des requins.



## Compléments

Si on veut avoir un affichage de l'évolution de la mer, il est peut être pertinent de ne pas afficher toutes les étapes mais une tous les 100 (par exemple) et de faire une petite pause après chaque affichage grâce au module `time` et sa fonction `sleep` :


   ```
   import time
   time.sleep(0.1) # pause de 1/10ème de seconde
   ```


On pourra ajouter à la simulation des variables globales qui comptabilisent à chaque instant les nombres de requins et de thons. On peut alors enregistrer à chaque pas de simulation le triplet *(numéro du pas, nombre de thons, nombre de requins)*.

Le résultat du programme peut alors être la liste de ces triplets pour tous les pas. Les données collectées dans cette liste peuvent être utilisées pour dessiner les courbes des évolutions de population. On peut pour cela utiliser le module `pylab`.

Tracer une courbe avec `pylab`est assez simple. Il suffit de faire :


   ```
   import pylab
       
   data_x = *une liste d'abscisses*
   data_y = *une liste d'ordonnées*
   pylab.plot(data_x, data_y)
   pylab.title('courbe des points de coordonnées (x,y)')
   pylab.show()
   ```   

Voici alors ce que l'on peut obtenir pour une mer de taille 25x25 et 125000 pas. On peut observer les cycles décalés d'évolution des populations (les thons sont en bleu et le requins en rouge) :

![Simulation 25*25](./images/evolution2.png)


**Nombre de pas** :
Pour le nombre de pas, on prend classiquement _largeur x hauteur  x N_ avec N de l'ordre de 100 ou 200.

_largeur x hauteur correspond statistiquement à "chaque case a évoluée une fois", donc le N correspond au nombre de cycles pour "toutes les cases"_



# Travail à réaliser

## 1. Créer deux constantes globales

**_Déclarez et initialisez_** les variables globales de la simulation. Ces variables permettront d'adapter la simulation à d'autres populations et d'afficher l'évolution de la mer.
 * Caractéristiques des poissons :
   * duree_gestation_thons (float)
   * energie_init_requins (float)
   * duree_gestation_requins (float)
 * Constantes pour les types de poissons (pour éviter les fautes de frappe):
   * THON = 'thon'
   * REQUIN = 'requin'
   * VIDE = None

## 2. Construire la mer

**_Réalisez_** une fonction `creerMer()` qui prend en paramètre le nombre de cases verticalement (nL = nombre de lignes), puis le nombre de cases horizontalement (nC = nombre de colonnes) et qui renvoie une liste de listes correspondant à une grille de la simulation. Toutes les cases sont vides (non occupées par un thon ou un requin). Pour représenter une case vide, la case contiendra La valeur None (utilisez la constante VIDE).

```python
>>> creerMer(2,3)
[[None, None, None], [None, None, None]]
```

_Note : La case de la mer référencée par [1][2] correspond à la case de la 2ème ligne et de la 3ème colonne_

**Remarque pour toutes les fonctions** : Toutes les fonctions doivent être documentées avec DocString et DocTest

## 3. Peupler la mer avec les thons et les requins

### Types de données pour les thons et les requins


Les thons et les requins seront représentés par des `dictionnaires` (<class 'dict'>). Les clés de chaque dictionnaire contiennent les caractéristiques du poisson.

Pour un Thon :
```python
thon = {'type': THON, 'gestation': 2}
```

Pour un requin :
```python
requin = {'type': REQUIN, 'gestation': 4, 'energie': 3}
```

Évidemment, lors de la création d'un poisson, il faut initialiser ses caractéristiques avec les valeurs communes à tous les requins ou thons (cf variables globales)

**_Réalisez_** une fonction `caractPoisson()` qui prend en paramètre le type de poisson 'THON' ou 'REQUIN'. Cette fonction renvoie la structure de données (dictionnaire) correspondant au type de poisson (avec ses caractéristiques initiales).

```python
>>> caractPoisson(THON)
{'type': 'thon', 'gestation': 2.0}
>>> caractPoisson(REQUIN)
{'type': 'requin', 'gestation': 3.0, 'energie': 6.0}
```

Vous allez avoir souvent besoin d'identifier les coordonnées des cases de la mer (n° de ligne et n° de colonne). A chaque fois que vous utiliserez des coordonnées de case en paramètre ou en valeur de retour de fonction, vous stockerez ces coordonnées dans un `tuple` (<class 'tuple'>)

_Remarque sur les tuples :_
>Les tuples sont des séquences immuables, ils sont indiqués pour référencer une case (ligne, colonne) car il n'y a pas de risque de modifier par inadvertance l'ordre des valeurs qu'ils contiennent (ça serait possible avec une liste). Ils s'utilisent comme des listes sauf qu'on ne peut pas modifier leurs valeurs.

**_Réalisez_** une fonction `creerPoisson()` qui prend en paramètre une grille de mer, un tuple (ligne, colonne) pour la coordonnée de la case et le type de poisson 'THON' ou 'REQUIN'. Pas de valeur de retour. Cette fonction doit insérer un nouveau poisson dans une case de la grille.

```python
>>> mer = creerMer(2, 3)
>>> creerPoisson(mer, (0,1), THON)
>>> mer[0][1]
{'type': 'thon', 'gestation': 2.0}
```

### Remplir la grille aléatoirement

**_Réalisez_** une fonction `remplirMer()` qui prend en paramètre une grille de mer, un pourcentage de Thon et un pourcentage de Requin. Le placement des poissons dans la grille doit être aléatoire. Cette fonction renvoie la grille remplie. Vous trouverez un peu d'aide après l'exemple.

```python
>>> mer = remplirMer(creerMer(2,5), 20, 30)
[[{'type': 'thon', 'gestation': 2.0}, {'type': 'thon', 'gestation': 2.0}, 
{'type': 'requin', 'gestation': 3.0, 'energie': 6.0}, None, None], 
[{'type': 'requin', 'gestation': 3.0, 'energie': 6.0}, None, 
{'type': 'requin', 'gestation': 3.0, 'energie': 6.0}, None, None]]
```

_Rappel :_ Un case de la mer est soit vide (None), soit contient un thon, soit contient un requin.

_Proposition pour remplir la grille :_
- Créer une liste locale de taille = nC*nL remplie de None
- Remplir cette liste avec les pourcentages de thon (chiffre 1) et de requin (chiffre 2)
- Mélanger cette liste
- Suivre l'ordre de cette liste pour remplir la mer en utilisant la division et le modulo pour trouver la bonne ligne et la bonne colonne

_Aide :_ Pour parcourir un liste en récupérant chacune des valeurs de la liste ainsi que les indices correspondants, on peut faire avec une boucle `while`, ou comme ceci :
```python
>>> maListe = list(range(100,110))
>>> for k, val in enumerate(maListe):
    print(k, val)
```

### Compter le nombre de thons et de requins
**_Réalisez_** une fonction `nombrePoissons()` qui prend en paramètre une grille de mer et le type de poisson 'THON' ou 'REQUIN' et qui renvoie le nombre de poisson du type demandé.

```python
>>> mer = remplirMer(creerMer(2,5), 20, 30)
>>> nombrePoissons(mer, THON)
2
>>> nombrePoissons(mer, REQUIN)
3
```

## 4. Sélectionner aléatoirement une case
Les différents pas de la simulation commencent par la sélection aléatoire d'une case de la mer.

**_Réalisez_** une fonction `selectionCase()` qui prend en paramètre une grille de mer et qui renvoie un tuple (ligne, colonne) pour la coordonnée de la case sélectionnée aléatoirement.

```python
>>> mer = creerMer(2,5)
>>> selectionCase(mer)
(0, 4)
>>> selectionCase(mer)
(1, 2)
```

Dans la simulation, il faut ensuite savoir ce que contient cette case sélectionnée.

**_Réalisez_** une fonction `typeOccupationCase()` qui prend en paramètre une grille de mer et un tuple de coordonnées et qui renvoie le contenu de la case : None, (string) THON ou (string) REQUIN

```python
>>> typeOccupationCase(mer, selectionCase(mer))
'thon'
```

**_Réalisez_** une fonction `occupantCase()` qui prend en paramètre une grille de mer et un tuple de coordonnées et qui renvoie le contenu de la case : None, un dictionnaire de thon (dict) ou un dictionnaire de requin (dict)

```python
>>> occupantCase(mer, selectionCase(mer))
{'type': 'requin', 'gestation': 6.0, 'energie': 3.0}
```



## 5. Trouver les cases voisines

Les comportements des thons et des requins sont régis par des règles à appliquer suivants l'occupation des cases voisines du poisson.

**_Réalisez_** une fonction `casesVoisines()` qui prend en paramètre une grille de mer et un tuple de coordonnées et qui renvoie une liste de tuples (ligne, colonne) pour les coordonnées des 4 cases voisines. `Les cases voisines sont retournées dans un ordre aléatoire`.

```python
>>> mer = remplirMer(creerMer(4,5), 30, 10)
>>> casesVoisines(mer, (2,2))
[(2, 1), (3, 2), (2, 3), (1, 2)]
>>> casesVoisines(mer, (2,2))
[(3, 2), (2, 3), (2, 1), (1, 2)]
```

**Rappel :**
>On considère la grille comme un tore. C'est à dire qu'une case en bas de la grille a pour voisine une case du haut de la grille, de même pour les bords gauche et droit. `Pensez à utiliser le modulo dans votre codage`.

![cases_voisines](./images/cases_voisines.png) ![cases_voisines_torique](./images/cases_voisines_torique.png) ![cases_voisines_torique_coin](./images/cases_voisines_torique_coin.png)


## 6. Trouver une case libre parmi les voisines

Les thons et les requins doivent choisir aléatoirement une case libre autour d'eux. Comme la fonction `casesVoisines()` renvoie la liste des cases voisines en ordre aléatoire, il suffit de trouver la première case libre parmi les cases voisines.

**_Réalisez_** une fonction `trouverCaseLibre()` qui prend en paramètre une grille de mer et une liste de tuples (ligne, colonne) pour les coordonnées des 4 cases voisines. Cette fonction renvoie un tuple de coordonnées pour la première case libre. S'il n'y a pas de case libre, la fonction renvoie -1.

>Pensez à utiliser les fonctions réalisées précédemment !

```python
>>> mer = remplirMer(creerMer(4,5), 50, 20)
>>> trouverCaseLibre(mer, casesVoisines(mer, (1,1)))
-1
>>> trouverCaseLibre(mer, casesVoisines(mer, (0,1)))
(0, 2)
```


## 7. Trouver un thon parmi les cases voisines

Les requins cherchent aléatoirement une case occupée par un thon autour d'eux. Comme la fonction `casesVoisines()` renvoie la liste des cases voisines en ordre aléatoire, il suffit de trouver la première case occupée par un thon parmi les cases voisines.

**_Réalisez_** une fonction `trouverThon()` qui prend en paramètre une grille de mer et une liste de tuples (ligne, colonne) pour les coordonnées des 4 cases voisines. Cette fonction renvoie un tuple de coordonnées pour la première case occupée par un thon. S'il n'y a pas de thon, la fonction renvoie -1.

>Pensez à utiliser les fonctions réalisées précédemment !

```python
>>> mer = remplirMer(creerMer(4,5), 50, 20)
>>> trouverThon(mer, casesVoisines(mer, (3,4)))
(0, 4)
>>> occupantCase(mer, (0,4))
{'type': 'thon', 'gestation': 2.0}
```


## 8. Déplacer un poisson d'une case

Les thons et les requins doivent pouvoir se déplacer d'une case à une autre. Quand ils se déplacent, la case de départ devient une case vide de la mer (elle contient None)

**_Réalisez_** une fonction `deplacerPoisson()` qui prend en paramètre une grille de mer, un tuple des coordonnées de départ et un tuple des coordonnées d'arrivée. Cette fonction doit déplacer le poisson de l'ancienne case à la nouvelle. L'ancienne case devient une case vide de la mer. Pas de valeur de retour.

```python
>>> occupantCase(mer, (0,4))
{'type': 'thon', 'gestation': 2.0}
>>> deplacerPoisson(mer, (0,4), (3,4))
>>> type(occupantCase(mer, (0,4)))
<class 'NoneType'>
>>> occupantCase(mer, (3,4))
{'type': 'thon', 'gestation': 2.0}
```

## 9. Gérer la gestation des poissons

Les temps de gestation des thons et des poissons sont différents mais la gestion de ce temps nécessite les mêmes fonctionnalités :

* diminuer le temps de gestation de 1
* remettre le temps de gestation à sa valeur initiale

**_Réalisez_** une fonction `diminuerGestation()` qui prend en paramètre une grille de mer et un tuple de coordonnées d'une case contenant un poisson.
Cette fonction diminue de 1 le temps de gestation d'un poisson et renvoie la nouvelle valeur de la gestation.

```python
>>> occupantCase(mer, (3,4))
{'type': 'thon', 'gestation': 2.0}
>>> diminuerGestation(mer, (3,4))
1.0
>>> occupantCase(mer, (3,4))
{'type': 'thon', 'gestation': 1.0}
```

**_Réalisez_** une fonction `reinitialiserGestation()` qui prend en paramètre une grille de mer, un tuple de coordonnées d'une case contenant un poisson et un type de poisson (THON ou REQUIN).
Cette fonction réinitialise le temps de gestation d'un poisson à sa valeur initiale et renvoie la nouvelle valeur de la gestation.

```python
>>> occupantCase(mer, (3,4))
{'type': 'thon', 'gestation': 1.0}
>>> reinitialiserGestation(mer, (3,4), THON)
2.0
```


## 10. Gérer l'énergie des requins

Les requins ont la particularité d'avoir un niveau d'énergie. La gestion de cette énergie nécessite les fonctionnalités suivantes :

* diminuer le niveau d'énergie de 1
* remettre le niveau d'énergie à sa valeur initiale 

**_Réalisez_** une fonction `diminuerEnergie()` qui prend en paramètre une grille de mer et un tuple de coordonnées d'une case contenant un requin.
Cette fonction diminue de 1 le niveau d'énergie d'un requin et renvoie la nouvelle valeur d'énergie.

```python
>>> occupantCase(mer, (1,2))
{'type': 'requin', 'gestation': 6.0, 'energie': 3.0}
>>> diminuerEnergie(mer, (1,2))
2.0
>>> occupantCase(mer, (1,2))
{'type': 'requin', 'gestation': 6.0, 'energie': 2.0}
```

**_Réalisez_** une fonction `reinitialiserEnergie()` qui prend en paramètre une grille de mer et un tuple de coordonnées d'une case contenant un requin.
Cette fonction réinitialise le niveau d'énergie du requin à sa valeur initiale et renvoie la nouvelle valeur d'énergie. 

```python
>>> config = {
    'duree_gestation_thons': 2.0,
    'energie_init_requins': 3.0,
    'duree_gestation_requins': 6.0
    }
>>> occupantCase(mer, (1,2))
{'type': 'requin', 'gestation': 6.0, 'energie': 2.0}
>>> reinitialiserEnergie(mer, (1,2), config)
3.0
```


## 11. Comportement complet du thon

Il est maintenant temps de mettre en œuvre le comportement complet d'un thon conformément à l'énoncé de la simulation.
Suivez scrupuleusement la description du comportement en respectant l'ordre des opérations.

**_Réalisez_** une fonction `comportementThon()` qui met en oeuvre le comportement complet d'un thon. Cette fonction prend en paramètre une grille de mer et un tuple de coordonnées d'une case contenant un thon. Pas de valeur de retour.

>Cette fonction fera évidemment appel aux autres fonctions déjà créées. 

```python
>>> comportementThon(mer, (0,3))
```

## 12. Comportement complet des requins

Il faut aussi mettre en œuvre le comportement complet d'un requin conformément à l'énoncé de la simulation.
Suivez scrupuleusement la description du comportement en respectant l'ordre des opérations.

**_Réalisez_** une fonction `comportementRequin()` qui met en oeuvre le comportement complet d'un requin. Cette fonction prend en paramètre une grille de mer et un tuple de coordonnées d'une case contenant un requin. Pas de valeur de retour.

>Cette fonction fera évidemment appel aux autres fonctions déjà créées. 

```python
>>> comportementRequin(mer, (1,2))
```

   
## 13. Démarrage de la simulation

Il vous reste enfin à réaliser une fonction qui lance la simulation !

À chaque pas de la simulation, une case de la mer est sélectionnée aléatoirement. Si elle est vide, il ne se passe rien. Si elle est occupée par un thon ou un requin, on applique alors le comportement correspondant.

Le résultat de chaque pas de simulation est enregistré dans une variable globale en tant que tuple :
```python
(numéro du pas, nombre de thons, nombre de requins)
```

**_Déclarez et initialisez_** la variable globale de la simulation dans laquelle vous enregistrerez chaque pas de la simulation.
```python
resultat_simul = []
```

**_Réalisez_** une fonction `simulationWaTor()` qui lance la simulation. Cette fonction prend en paramètre :
- le nombre de lignes et de colonnes de la grille de mer,
- le pourcentage de thon et de requin, 
- le nombre de pas de la simulation,
- une liste qui contiendra les données de la simulation,

Cette fonction doit respecter l'algorithme suivant :
  1. Créer et remplir aléatoirement la mer
  2. À chaque pas de la simulation, 
      - une case de la mer est sélectionnée aléatoirement. 
      - Si elle est vide, il ne se passe rien. Si elle est occupée par un thon ou un requin, on applique alors le comportement correspondant.
      - Ajouter un tuple (numéro du pas, nombre de thons, nombre de requins) dans la liste des résultats.



## 14. Affichage des courbes

**_Réalisez_** une fonction `afficherCourbes()` qui lance l'affichage des courbes de la simulation. Cette fonction prend en paramètre une liste qui contient les données de la simulation.

Sur le même graphique `pylab`, vous tracerez :
  - Une courbe pour l'évolution de la population de thons
  - Une courbe pour l'évolution de la population de requins

![Contenu de la mer](./images/courbes_evolution_gwen.png)


## 15. Affichage du contenu de la mer dans l'interpréteur

Vous pouvez afficher dans l'interpréteur python l'évolution du contenu de la mer pendant la simulation.

**_Réalisez_** une fonction `afficherMerConsole()` qui prend en paramètre une grille de mer. Cette fonction devra représenter le contenu de la mer :
  - Les cases vides seront affichées avec un tiret (-)
  - Les cases contenant un THON seront affichées avec un o majuscule (O).
  - Les cases contenant un REQUIN seront affichées avec un x majuscule (X).
  - Le contenu des cases sera séparé par une espace.
  - Chaque ligne de la grille sera affichée sur une ligne distincte.

**_Modifiez_** la fonction `simulationWaTor()` en lui ajoutant un dernier paramètre `afficher=False`. Ajouter l'appel à la fonction `afficherMerConsole()` tous les 100 pas de simulation et seulement si `afficher == True`.

![Contenu de la mer](./images/affichage_mer_25_25.png)


