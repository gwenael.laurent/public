# WSL : Windows Subsystem for Linux

> * Auteur : Gwénaël LAURENT
> * Date : 25/12/2020
> * OS : Windows 10 (version 1903)
> * VScode : version 1.52.1 (system setup)

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

- [WSL : Windows Subsystem for Linux](#wsl--windows-subsystem-for-linux)
  - [1. Activer WSL](#1-activer-wsl)
  - [2. Installer une distribution Ubuntu](#2-installer-une-distribution-ubuntu)

## 1. Activer WSL
Paramètres > Application > Programmes et fonctionnalités > Activer ou désactiver des fonctionnalités Windows
* Cocher : "Sous-système Windows pour Linux"
* Redémarrer Windows

## 2. Installer une distribution Ubuntu
Dans le Microsoft Store > Chercher "Ubuntu"
* Cliquer sur "Ubuntu" (la description indique que c'est une version 20.04 LTS)
* Cliquer sur "