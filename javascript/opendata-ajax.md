# Services web OpenData et consommation en AJAX

> * Auteur : Gwénaël LAURENT
> * Date : 04/05/2020
> * OS : Windows 10 (version 1903)
> * VScode : version 1.44.2 (system setup)

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

- [Services web OpenData et consommation en AJAX](#services-web-opendata-et-consommation-en-ajax)
- [C'est quoi l'OpenData](#cest-quoi-lopendata)
- [C'est quoi un service web](#cest-quoi-un-service-web)
  - [Définition](#d%c3%a9finition)
  - [Protocole HTTP](#protocole-http)
  - [Format JSON](#format-json)
- [Quelques services web OpenData](#quelques-services-web-opendata)
  - [OpenFoodFact](#openfoodfact)

# C'est quoi l'OpenData
> **Définition : L'OpenData permet de réutiliser les informations publiques à d’autres fins que celles pour lesquelles elles sont détenues ou élaborées**

Les données sont souvent disponibles sous la forme de **fichiers à télécharger** (format Excel, XML, ...). Le problème c'est que les données sont "figées" au moment où on a téléchargé le fichier. Si on veut des données plus récentes, il faut re-télécharger la dernière version du fichier. Par exemple, c'est embêtant si on veut incorporer ces données dans notre site web. Imaginez ce qui se passerait si on veut afficher la météo du jour ...

[Téléchargement de données OpenData sur data.gouv.fr](https://www.data.gouv.fr/fr/)

Certains fournisseur OpenData pemettent aussi d'avoir accès à leurs données via un **service web**. L'avantage de ce système c'est qu'à chaque fois qu'on fait une requête pour obtenir des données, c'est les dernières données que nous récupérons (les données les plus à jour). 
Pour reprendre l'exemple de notre site web qui incorpore les données d'un fournisseur OpenData : au lieu de faire une copie des données dans l'arborescence de notre site web, on va utiliser une technique (AJAX) qui intérroge le service web à chaque affichage de notre page. Les données reçues du service web seront alors intégrées dans notre page web grâce aux techniques standards de Javascript. 

[Services web OpenData sur public.opendatasoft.com](https://public.opendatasoft.com/)

> Evidemment,c'est les services web qui nous intéressent ici.

# C'est quoi un service web
## Définition
> **Définition : une service web est un programme hébergé sur un serveur web qui renvoie des données (plutôt qu’une page web)**

![Arcitecture service web](img-opendata/architecture-service-web.png)

## Protocole HTTP
Comme les services web sont hébergés sur un serveur web, les requêtes envoyées aux services web utilisent aussi le **protocole HTTP**. 

Chaque trame HTTP (requête ou réponse) contient les en-têtes des protocoles réseaux utilisés.

![Trame HTTP](img-opendata/trame-http.png)

Le protocole HTTP est composé de 2 parties :
* **L'en-tête HTTP** : qui contient des données destinées au serveur web ou au client web
* **La donnée HTTP** : 
  * Pour un serveur web classique c'est par exemple le code HTML contenu dans la page web demandée.
  * Pour un service web c'est les données brutes renvoyées par le service web.

> Le Javascript permet de manipuler l'en-tête et les données HTTP.

Le protocole HTTP utilise un en-tête au format "texte brut".

![requete réponse en HTTP](img-opendata/http-requete-reponse.png)

* Pour les **requêtes** (du client vers le serveur web), il contient :
  * le type de requête : GET, POST, PUT, DELETE, ...
  * l'identification de la ressource demandée
  * des données concernant le client
  * éventuellement des données qui serviront de paramètres à la requête (un peu à la manière des paramètres d'une fonction)
    ```
    GET /recherche.php?recMot=lycee&submit=&type=Simple&recPer=per HTTP/1.1
    Host: www1.ac-lille.fr
    Connection: keep-alive
    Upgrade-Insecure-Requests: 1
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
    Accept-Encoding: gzip, deflate
    Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7
    Cookie: _ga=GA1.2.xxxxxxxxxxxxxxx
    ```
* Pour les **réponses** (du serveur au client), il contient :
  * un code de status qui indique comment le serveur a interprété la demande (ex 200 : Le serveur web a trouvé la page demandée, 404 : le serveur n'a pas trouvé la ressource demandée, ...)
  * des données concernant le serveur
    ```
    HTTP/1.1 200 OK
    Server: nginx
    Date: Mon, 04 May 2020 11:45:37 GMT
    Content-Type: text/html; charset=UTF-8
    Transfer-Encoding: chunked
    Connection: keep-alive
    Expires: Thu, 19 Nov 1981 08:52:00 GMT
    Pragma: no-cache
    Cache-Control: max-age=900
    X-Varnish-TTL: 900.000
    Age: 0
    X-Cache: MISS
    Accept-Ranges: bytes
    ```

## Format JSON
Les données renvoyées par un service web sont souvent écrites au **format JSON** (JavaScript Object Notation). 

```json
[
  {
    "Id": "3",
    "Addr": "62400 BÉTHUNE"
  },
  {
    "Id": "28",
    "Addr": "59280 ARMENTIERES"
  }
]
```

* On retrouve des tableaux de données : reconnaissables aux crochets **```[```** et **```]```**
* On retrouve des objets : reconnaissables aux accolades **```{```** et **```}```**
* Les objets comportent des propriétés avec leur valeur : 
  * les propriétés et leur valeur sont toujours entourées de double-quote **```"```**...**```"```**
  * les propriétés et leur valeurs sont toujours séparées par un double point **```:```**

# Quelques services web OpenData

## OpenFoodFact

