# Créer un projet Javascript qui utilise jQuery

> * Auteur : Gwénaël LAURENT
> * Date : 25/03/2020
> * OS : Windows 10 (version 1903)
> * VScode : version 1.43.0 (system setup)

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

> Ce document remplace la ```page 7``` du document ```JS2  Langage Javascript jQuery```.
> Il explique comment intégrer la bibliothèque jQuery dans un projet créé ```avec VScode``` et détaille beaucoup plus le fonctionnement

- [Créer un projet Javascript qui utilise jQuery](#cr%c3%a9er-un-projet-javascript-qui-utilise-jquery)
  - [1. C'est quoi jQuery ?](#1-cest-quoi-jquery)
  - [2. Création d'un projet Javascript](#2-cr%c3%a9ation-dun-projet-javascript)
  - [3. Téléchargement et intégration de jQuery](#3-t%c3%a9l%c3%a9chargement-et-int%c3%a9gration-de-jquery)
  - [4. Le code HTML pour charger jQuery](#4-le-code-html-pour-charger-jquery)
  - [5. Le DOM c'est quoi ?](#5-le-dom-cest-quoi)
  - [6. Utiliser jQuery dans votre code Javascript](#6-utiliser-jquery-dans-votre-code-javascript)
  - [7. Explication du code](#7-explication-du-code)
    - [7.1 ```$(document)```](#71-document)
    - [7.2 La fonction ```ready(...)```](#72-la-fonction-ready)
  - [8. Télécharger l'architecture de base d'un projet jquery](#8-t%c3%a9l%c3%a9charger-larchitecture-de-base-dun-projet-jquery)
  - [8. Intellisense VScode pour jQuery](#8-intellisense-vscode-pour-jquery)

## 1. C'est quoi jQuery ?
jQuery est une  bibliothèque JavaScript qui se concentre sur la simplification de la manipulation de HTML, les appels d'AJAX et la gestion des évènements. 

## 2. Création d'un projet Javascript
Dans VScode, créez un projet Javascript standard avec cette arborescence :

![arborescence standard](img-creer-jquery/arbo-standard.png)

Le fichier **monscript.js** contiendra votre code Javascript.

> Evidemment, les noms de fichiers doivent correspondrent à votre projet. A la place de ```monscript```.js et ```monstyle```.css, utilisez un mot qui "résume" le but de votre projet : *moyenne*.js, *conv-temperature*.css, *syracuse*.js, ...

## 3. Téléchargement et intégration de jQuery
La bibliothèque jQuery se compose d'un seul fichier téléchargeable à l'adresse [https://jquery.com/download/](https://jquery.com/download/). 

Il existe 2 versions de la bibliothèque jQuery :
* **```Version compressée```** : à utilisé quand le site web est déployé
  * Cliquez sur le lien *"Download the compressed, production jQuery 3.4.1"*
* **```Version NON compressée```** : à utilisé pendant le développement du site web
  * Cliquez sur le lien *"Download the uncompressed, development jQuery 3.4.1"*

> *J'imagine que vous avez compris que vous allez télécharger la version ```NON compressée``` parce que vous êtes en train de coder votre site web (et que du coup, vous aimeriez bénéfier d'aide pendant le codage pour l'utilisation des fonctions de jQuery)*

Donc, téléchargez la version ```NON compressée``` (Clic droit sur le lien > Enregistrer le lien sous ...) et enregistrez le fichier ```jquery-3.4.1.js``` dans le dossier ```js```de votre projet.

![arborescence avec jquery](img-creer-jquery/arbo-jquery.png)

> Une copie de la version non compressée est également disponible [sur ce site](projets-jquery/js/jquery-3.4.1.js).


## 4. Le code HTML pour charger jQuery
La bibliothèque jQuery est un fichier qui contient du code Javascript (*10598 lignes de code dans la version 3.4.1 non compressée*).

Donc comme tout fichier Javascript, il faut ajouter dans le HTML une balise ```<script>``` pour que le navigateur web sache qu'il doit charger ce fichier.

> **ATTENTION** : il faut absolument charger la bibliothèque jQuery **```AVANT```** votre fichier monscript.js. Sinon vous ne pourrez pas utiliser les fonctions jQuery dans votre codage.

Voici une structure de code HTML qui charge les deux fichiers Javascript (juste avant la balise de fermeture ```</body>```)

```html
<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tests du documents JS2</title>
        <link rel="stylesheet" href="css/monstyle.css">
    </head>

    <body>
        <h1>Tests du documents JS2</h1>
        <!-- votre code HTML ici-->
        <script src="js/jquery-3.4.1.js"></script>
        <script src="js/monscript.js"></script>
    </body>

</html>
```

## 5. Le DOM c'est quoi ?
> **A savoir** : Le code HTML chargé dans la mémoire du navigateur web s'appelle le **```DOM```** (**```Document Object Model```**). 

Quand un navigateur web doit afficher une page HTML :
  1. Il télécharge le fichier HTML
  2. Il parcourt le code HTML contenu dans le fichier
  3. Chaque balise HTML est enregistrée en mémoire ```dans le DOM``` (*en respectant l'imbriquation des balises*)
  4. Le rendu graphique de la balise est affiché à l'écran en correspondance avec le contenu du DOM

> **A savoir** : Grâce au code Javascript, on peut modifier directement le DOM. Toute modification effectué dans le DOM se traduit automatiquement par la modification du rendu graphique à l'écran.

**Remarque** : le code Javascript peut modifier le DOM, mais ne modifiera jamais le fichier HTML.

Le navigateur web Chrome vous permet de voir le contenu du DOM. C'est pratique, parce que vous pourrez voir "en live" les modifications apportés par votre code Javascript quand vous débuguer en mode pas-à-pas.

Affichez votre page ```index.html``` dans Chrome et appuyez sur la touche ```F12``` de votre clavier, puis cliquez sur ```Elements```. Le code affiché, c'est le DOM.

![F12 dans Chrome](img-creer-jquery/f12-chrome.png)

## 6. Utiliser jQuery dans votre code Javascript
> cf les ```page 6, 8 et 9``` du document ```JS2  Langage Javascript jQuery```.

> **A savoir** : Quand un navigateur web charge un fichier Javascript, il exécute immédiatement le code contenu dans le fichier, même si le code HTML n'est pas complètement chargé en mémoire (= le DOM n'est pas complet)!

**Problème potentiel** : Et bien, ça peut générer des erreurs pendant l'exécution de votre code Javascript. Ca se produit si votre code cherche à lire / modifier le contenu d'une balise HTML et que le navigateur ne la connait pas encore.

Un des intérêts de la bibliothèque jQuery, c'est de fournir un moyen pour attendre que tout le code HTML soit chargé en mémoire avant d'exécuter votre code Javascript. Donc, plus de problème de ce côté là.

Pour activer cette fonctionnalité d'attente, votre fichier ```monscript.js``` doit **TOUJOURS contenir** :

```js
$(document).ready(function () {
    //Tout votre code Javascript ici
    // ...
    // ...
});
```

## 7. Explication du code

### 7.1 ```$(document)```
Remarquez le ```$(...)``` au début du code : Les parenthèses nous font penser à l'appel d'une fonction. Quel est le nom de cette fonction ? Le nom n'est composé que d'un seul caractère, le ```$``` . Et bien, cette fonction qui s'appelle ```$``` c'est le petit nom de la biblithèque ```jQuery``` ! En fait, jQuery c'est une fonction ! (*certes, une énorme fonction*). 

> Remarque : la fonction ```$(...)``` peut aussi s'écrire ```jQuery(...)``` mais c'est plus long et les codeurs sont fainéants, donc c'est rarement utilisé !

Ici, l'argument de la fonction est ```document```. On peut passer à la fonction jquery une multitude d'arguments différents. **Ces arguments permettent de sélectionner une partie du code HTML affiché dans le navigateur**. Dans le cas présent,  ```document``` désigne ```TOUTE la page HTML``` chargée dans le navigateur.

> **A savoir** : Etant donné son rôle, l'argument passé à la fonction ```$(...)``` s'appelle un **```sélecteur```**, car il nous permet de sélectionner une partie du code HTML.

> **A savoir** : En fonction de l'argument qu'on lui passe, la fonction ```$(...)``` va renvoyer une variable Javascript qui nous permettra de manipuler cette partie du code HTML (*une balise HTML , plusieurs balises HTML, la page HTML entière* ... cela dépend de l'argument).

Donc, pendant l'exécution du code, la partie ```$(document)``` sera remplacée par une variable qui sera (dans le cas ci) de type ```objet jQuery```. Et bien, ces objets jQuery sont des variables complexes qui ```permettent de manipuler le DOM```. Pour ce faire, les objets jQuery contiennent des **```propriétés```** (= les variables internes à cet objet) et des **```fonctions```** (qui agissent sur cet objet).

On pourrait donc très bien écrire un code équivalent sous la forme :
```js
let objetDoc = $(document);
objetDoc.ready(function () {
    //Tout votre code Javascript ici
    // ...
    // ...
});
```

### 7.2 La fonction ```ready(...)```
L'objet jQuery récupéré grâce au sélecteur ```$(document)``` contient une fonction ```ready(...)```. Cette fonction permet d'**attendre que le DOM soit complètement chargé dans le navigateur**. 

> **A savoir** : jQuery utilise un format ```$(selecteur).action()``` pour effectuer une action sur un élément sélectionné.

Comment est-on averti quand le DOM est complètement chargé ? 

Grâce à l'argument passé à la fonction ```ready(...)```. C'est un argument un peu particulier, car il s'agit d'une déclaration de fonction, et cette fonction sera appelée (=exécutée) quand le DOM sera complètement chargé.

```js
function () {
    //Tout votre code Javascript ici
    // ...
    // ...
}
```
Avez-vous remarqué ce qu'il manque par rapport aux déclarations de fonctions standards ?

Cette fonction n'a pas de nom ! Pourquoi ? Parce qu'elle n'a pas besoin d'être appelée par son nom, elle est déclarée directement comme argument de ready(...). On parle alors de ```fonction anonyme```

> **Attention** : cette fonction ne doit pas être exécutée en même temps que la fonction ready(). C'est la valeur de retour de ready() qui déclenchera l'exécution de cette autre fonction. On parle alors de fonction de ```callback``` (=appelée en retour)

Remarque : On pourrait tout à fait utiliser une fonction avec un nom. On pourrait écrire un code équivalent sous la forme :

```js
$(document).ready(lancer_programme);

function lancer_programme() {
    //Tout votre code Javascript ici
    // ...
    // ...
}
```
Remarquez qu'à la première ligne, l'argument de la fonction ready() est le nom de la fonction lancer_programme ```SANS PARENTHESES```. S'il y avait des parenthèses (lancer_programme```()```) la fonction serait exécutée en même temps que ready().

## 8. Télécharger l'architecture de base d'un projet jquery
[Architecture d'un projet avec jQuery](projets-jquery/)

## 8. Intellisense VScode pour jQuery
L'intellisense pour jQuery ne s'active pas automatiquement dans VScode. 

La façon la plus simple de l'activer est d'ouvrir dans l'éditeur le fichier jquery-3.4.1.js. Ca active l'intellisense pour les autres fichiers Javascript du projet.