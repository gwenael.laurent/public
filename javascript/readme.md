# Ressources pour la programmation Javascript

> * Auteur : Gwénaël LAURENT
> * Date : 26/03/2020
> * OS : Windows 10 (version 1903)

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

![Javascript logo](img/logo-js.png)

* [VScode pour coder du JavaScript](../vscode/vscode-js.md)
* [Créer un projet Javascript qui utilise jQuery](creer-projet-jquery.md)