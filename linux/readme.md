# Ressources pour Linux

> * Auteur : Gwénaël LAURENT
> * Date : 13/10/2021


![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

# C'est quoi Linux ?
**```Système d’exploitation```** : OS (Operating System) : ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur

**```Linux```** : OS fondé sur le noyau Linux (kernel)
Le noyau Linux a été créé en 1991 par Linus Torvalds (à 22 ans) université d'Helsinki, Finlande. Linux est un OS dérivé de UNIX.

**```Distribution Linux```** : ensemble cohérent de logiciels assemblés autour du noyau Linux (Debian, Ubuntu, SUSE, Red Hat, … )


# Ressources

* [1. Installer Ubuntu 20.04.2 Desktop](1.installer_ubuntu_20.04.2_Desktop.md)
* [2. Introduction à l'interpréteur de commandes UNIX (doc de cours)](2.introduction_shell_unix.md)
* [3. TP Droits et permissions d'accès aux fichiers](3.TP__droits_permissions_acces.md)
* [4. Installation de LAMP sur Linux (distribution Debian Buster)](4.installer-LAMP.md)
* [5. Installer le broker MQTT mosquitto](5.installer_broker_mqtt_mosquitto.md)



