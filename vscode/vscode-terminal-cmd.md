# Utiliser l'invite de commande CMD directement dans VScode

> * Auteur : Gwénaël LAURENT
> * Date : 29/09/2019
> * OS : Windows 10 (version 1903)
> * VScode : version 1.38.1 (system setup)
> * Node.js : version 10.16.3

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)


Dans VScode, ouvrez un terminal (Invite de commande Windows directement dans VScode) à partir du menu principal : Terminal > New 

![New terminal](img/new-terminal.png)

En bas à droite,  modifiez le terminal par défaut :

![New terminal](img/select-default-shell.png)

En haut, une fenêtre s'ouvre, sélectionnez : "**Command Prompt** C:\WINDOWS\System32\\```cmd.exe```"

![New terminal](img/command-prompt-cmd.png)

Fermez le terminal ouvert :

![Fermer le terminal](img/fermer-terminal.png)

Et réouvrez un nouveau Terminal (le terminal doit être de type **cmd**)

![Terminal CMD](img/terminal-cmd.png)