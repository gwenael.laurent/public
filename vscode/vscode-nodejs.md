# VScode pour coder du Node.js
# ```_**En cours de rédaction et test**_```

> * Auteur : Gwénaël LAURENT
> * Date : 24/07/2023
> * OS : Windows 10 22H2
> * Node.js : version 18.17.0 LTS

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

> Documentation officielle ["Node.js tutorial in Visual Studio Code"](https://code.visualstudio.com/docs/nodejs/nodejs-tutorial)
> * [Debugging](https://code.visualstudio.com/docs/editor/debugging)
> * [Node.js debugging in VS Code](https://code.visualstudio.com/docs/nodejs/nodejs-debugging)

# 1. Installation
Il faut juste installer l'interpréteur Node.js et le linter ESlint.

Suivez le tuto [Installation de Node.js](../nodejs/installer_nodejs.md)


# 2. Hello World
## 2.1 Création d'un dossier pour le projet dans VScode
Même technique que pour les projets HTML, il faut créer un dossier sur votre disque dur et l'ouvrir dans VScode.

