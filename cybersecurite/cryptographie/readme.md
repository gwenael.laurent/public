# Ressources en cryptographie

> * Auteur : Gwénaël LAURENT
> * Date : 11-01-2025

![CC-BY-NC-SA](../../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)


* [01.Généralités sur la cryptographie](01.generalites-cryptographie.md)
* [02.Le hashage pour l'intégrité des données](02.hashage-integrite.md)
* [03.Chiffrement symétrique pour la confidentialité](03.chiffrement-symetrique.md)
* [04.Chiffrement asymétrique pour la confidentialité](04.chiffrement-asymetrique.md)
* [05. Signature numérique pour l'authenticité](05.signatures.md)
* [06. Certificats électroniques pour l'authenticité](06.certificats.md)

* [10.OpenSSL : présentation et installation](10.openssl-installation.md)
* [11.OpenSSL : utilisation](11.openssl-utilisation.md)