# TP shell 2 : Droits et permissions d'accès aux fichiers
Auteur : Gwénaël LAURENT

## Sujets abordés
* `Droits et permissions d'accès aux fichiers`

## Préparation / besoins techniques
Un ordinateur par élève ou binôme avec un `OS Ubuntu Desktop` dans une machine virtuelle `Virtualbox`. Un compte utilisateur d'élève et deux utilisateurs supplémentaires : user1 (pass = user1) et user2 (pass = user2)

# Eléments de cours
Consultez le document ["Très et trop brève introduction à UNIX, à l'interpréteur de commandes"](ressources/shell-bref.md)

Consultez également [Wikipédia : Permissions UNIX](https://fr.wikipedia.org/wiki/Permissions_UNIX)

Vous trouverez enfin un résumé sur la [carte de référence Unix de Moïse Valvassori](ressources/unix-refcard.pdf)

# Travail à réaliser

> A partir de maintenant, toutes les manipulations du TP doivent être réalisées via l'interpréteur de commandes, dans un terminal. Vous trouverez un terminal dans la barre d'icônes de l'environnement graphique de Linux. 

![icône d'un terminal](images/terminal.png)

## Quelques rappels sur les commandes de base
Logguez vous avec votre compte linux et ouvrez un terminal.

1. Donnez la commande pour afficher le répertoire courant.
```


```
2. Donner la commande pour afficher (sans changer de répertoire courant) le contenu du répertoire racine.
```


```
3. Donner la commande pour vous déplacer dans le répertoire `etc` en utilisant un chemin absolu
```


```
4. Donner la commande pour vous déplacer vous dans votre répertoire personnel en utilisant un nom relatif
```


```
## Utilisateurs et groupes
Chaque personne qui utilise un système UNIX est authentifié par un `utilisateur`. Un utilisateur est reconnu par un nom unique et un numéro unique **UID** (la correspondance nom/numéro est stockée dans le fichier `/etc/passwd`).

Un utilisateur UNIX appartient à un ou plusieurs `groupes`. Les groupes servent à rassembler des utilisateurs afin de leur attribuer des droits communs. Un groupe est reconnu par un nom unique et un numéro unique **GID** (la correspondance nom/numéro et la liste des utilisateurs membres de chaque groupe sont stockées dans le fichier `/etc/group`)

La commande `id` permet de visualiser votre identifiant d'utilisateur (`uid`), votre groupe principal (`gid`) ainsi que les   groupes auxquels vous appartenez. 

Donnez le résultat de la commande `id`
```


```
> Remarque : La commande `id user1` permet d'afficher le même type d'information pour l'utilisateur `user1`

Donnez les commandes qui permettent d'afficher le contenu des fichiers `/etc/passwd` et `/etc/group`
```


```
A partir du contenu de ces fichiers, en vous inspirant de la ligne correspondant à votre utilisateur, donnez l'UID de l'utilisateur `root`
```


```
> L’utilisateur root (racine) est un utilisateur particulier qui dispose de tous les droits sur le système, on dit qu'il dispose des droits d'administration.

## Propriétaires des fichiers
Tout fichier UNIX possède un `utilisateur propriétaire`. Au départ, c'est l'utilisateur qui a créé le fichier. Seul le propriétaire du fichier et le super utilisateur (root) peuvent changer les droits.

Un fichier UNIX appartient aussi à un `groupe propriétaire`. Ceci donne pleinement son sens à la notion de groupe. On définit ainsi les actions du groupe sur ce fichier.

Pour afficher les propriétaires d'un fichier ou d'un dossier, on utilise la commande `ls -l nom_du_fichier` :

![Permissions UNIX](images/permissions.png)

Donnez les propriétaires du fichier `/etc/passwd`.
```





```
Vous allez créé un fichier `message` à la racine de votre répertoire personnel. Ce fichier contiendra le texte `coucou`. Utilisez la commande suivante :
`echo Coucou > ~/message`

> La commande `commande > fichier` permet de rediriger la sortie standard de la commande vers l'entrée standard du fichier. Vous pouvez consulter [Redirection des entrées sorties](https://www.funix.org/fr/unix/commandes.htm)

Donnez les propriétaires du fichier `message`.
```





```

## Les droits d'accès aux fichiers
Les droits d'accès gèrent aussi quelles actions les utilisateurs ont le droit d'effectuer sur les fichiers (lecture **R**, écriture **W** et exécution **X**), selon qu'ils sont propriétaire du fichier, membre du groupe propriétaire du fichier ou ni l'un ni l'autre (_les autres_). 

> Les permissions d'accès aux fichiers suivent la norme [POSIX](https://fr.wikipedia.org/wiki/POSIX), elles sont inspirées des permissions d'accès UNIX.

![Droits d'accès](images/droits_acces.png)

Dans cet exemple : 
* L'utilisateur propriétaire `localuser` a les droits `rw-` donc il a les droits de lecture et de modification (et pas d'exécution) sur le fichier.

* Le groupe propriétaire `NSI` a les droits `r--` donc tous les utilisateurs membres du groupe `NSI` ont seulement le droit de lire le fichier (pas de modification ni d'exécution).

* Les autres utilisateurs (`Others`) ont les droits `---`, donc les utilisateurs qui ne sont pas `localuser` et qui ne font pas partie du groupe `NSI` n'ont aucun droit d'accès sur le fichier (ni lecture, ni modification, ni d'exécution)

Avez-vous le droit de modifier le contenu du fichier `/etc/passwd` ? Justifiez votre réponse en utilisant les commandes précédentes.
```









```

## Les répertoires : un cas particulier
![Droits d'accès des dossiers](images/droits_acces_dossier.png)

Pour un répertoire, les droits d'accès `rwx` ont une signification particulière :

* `r` signifie qu'on peut afficher la `liste des fichiers` contenus à la racine de ce répertoire (avec `ls` par exemple)
* `w` signifie qu'on peut `écrire` dans ce répertoire (création, suppression, renommage des fichiers)
* `x` signifie qu'on peut `accéder` au contenu du répertoire (pour ouvrir un fichier ou se déplacer dans un sous-répertoire)

L'utilisateur `user1` peut-il accéder à votre répertoire personnel ? Quels sont précisémment ses droits d'accès ? Justifiez votre réponse.
```









```

## Représentation des droits
### Forme symbolique 'ugo'
Les droits sont affichés par une série de 9 caractères (rwxrwxrwx). Associés 3 par 3 (rwx rwx rwx) ils définissent les droits des 3 identités (`u` = user, `g` = group et `o` = others).

![Droits d'accès UGO](images/droits_acces_ugo.png)

### Forme numérique
Une autre manière de représenter ces droits est sous forme `octale`. Chaque « groupement » de droits `rwx` sera représenté par un chiffre et à chaque droit correspond une valeur.
* r  = 4
* w  = 2
* x  = 1
* \- = 0

![Droits d'accès binaire](images/droits_acces_octal.png)

Donnez la représentation numérique des droits d'accès de `/root`, `/etc`, `/etc/shadow` et de votre répertoire personnel
```






```

## Changement des droits et des propriétaires
> Pour l'utilisation des commandes chmod et chown, consultez le [Wiki Ubuntu-fr : Gérer les droits d'accès §3 Modifier les permissions](https://doc.ubuntu-fr.org/permissions#modifier_les_permissions)

### Changements des droits : **chmod**
Le changement de droits s'effectue avec la commande `chmod` (_change mode_).

#### Exemples avec la forme symbolique

Enlever le droit d'écriture pour les autres.
```
chmod o-w fichier3
```
On peut aussi combiner plusieurs actions en même temps :
```
chmod u+rwx,g+rx-w,o+r-wx fichier3
```
* On ajoute la permission de lecture, d'écriture et d'exécution sur le fichier fichier3 pour le propriétaire
* On ajoute la permission de lecture et d'exécution au groupe propriétaire, on retire la permission d'écriture
* On ajoute la permission de lecture aux autres, on retire la permission d'écriture et d'exécution.

#### Exemples avec la forme numérique

Affecter les droits `rwxr-x---` au fichier3 :
```
chmod 750 fichier3
```

### Changement des proprétaires : **chown**
Le changement de propriétaire ou de groupe propriétaire s'effectue avec la commande `chown` (_change owner_).

Exemple pour changer en une seule commande le propriétaire et le groupe du fichier :
```
chown toto:lesPotes fichier1
```
Pour ne changer que le proprétaire :
```
chown toto fichier1
```
Pour ne changer que le groupe proprétaire :
```
chown :lesPotes fichier1
```


## Exercice d'application
--------------------
Dans votre répertoire personnel, créez l'arborescence de dossiers suivante. Vous donnerez les commandes utilisées.
```
fichiers/
├── partage
└── prive
```

```






```

Créez ensuite deux fichiers et donnez les commandes utilisées :
* Le fichier `fichier_readme` est créé dans le dossier `partage` et contient le texte "A lire créé par votre_nom_de_login`"
* Le fichier `fichier_prive` est créé dans le dossier `prive` et contient le texte "Le fichier appartient à votre_nom_de_login`"
```
fichiers/
├── partage
│   └── fichier_readme
└── prive
    └── fichier_prive
```

```





```

Ouvrez une deuxième fenêtre de terminal. Changez d'utilisateur dans ce terminal pour être identifié comme `user1`. Utilisez la commande :
```
% su - user1
```
Donnez le répertoire courant dans le 2ème terminal (commande + résultat)
```



```
L'utilisateur `user1` peut-il lire les fichiers `fichier_prive` et `fichier_readme`? Donnez les commandes utilisées.
```









```
Ouvrez un troisième terminal et logguez vous en `user2`. L'utilisateur `user2` peut-il lire les fichiers `fichier_prive` et `fichier_readme` ? Justifiez votre réponse.
```









```

Pour la suite de l'exercice, vous réaliserez toujours les modifications avec votre utilisateur courant et vous utiliserez `user1` pour tester vos modifications.

En utilisant la **forme symbolique**, interdisez l’accès au répertoire `prive` pour les membres du groupe `NSI` et les autres. Donnez les commandes utilisées.
```


```

Le fichier `fichier_readme` devra être consultable mais non modifiable par les membres du groupe `NSI` et non lisible/modifiable par les autres. Modifiez les droits en 	utilisant la **forme numérique**. Donnez les commandes utilisées et/ou justifiez votre réponse.

> Pour modifier le contenu d'un fichier, vous pouvez utiliser la commande suivante qui ajoute  le texte "Coucou" à la suite du texte déjà présent dans le fichier.

```
% echo Coucou >> fichier_readme
```

```







```

Dans le répertoire `partage` créez un fichier `fichier_ecriture` qui contient le texte "Ecriture créé par votre_nom_de_login". Ce fichier devra être consultable et modifiable par les membres du groupe `NSI` mais pas par les autres. Modifiez les droits en utilisant la **forme numérique**. Donnez les commandes utilisées.
```
fichiers/
├── partage
│   ├── fichier_ecriture
│   └── fichier_readme
└── prive
    └── fichier_prive
```

```










```

Dans le répertoire `prive` créez un fichier `salut` avec le contenu suivant :
```
echo Hello World
```
> Le contenu du fichier est une commande linux. On parle alors de fichier `script`.

Tentez de l'exécuter en tapant `./salut`. Modifiez les droits sous **forme symbolique** de manière à ce que vous puissiez l'exécuter puis vérifiez que vous pouvez l'exécuter. Donnez les commandes utilisées.
```









```
