# E3C QCM d'épreuves communes de contrôle continu
Auteur : Gwénaël LAURENT

## Thème
1. Types et valeurs de base
2. Types construits
3. Traitement de données en tables
4. Interaction Homme-machine sur le web
5. __Architectures matérielles et systèmes d'exploitation__
6. Langages et programmation
7. Algorithmique


## Sujets abordés
* `TP shell 2` : [Droits et permissions d'accès aux fichiers](tp_shell_2_droits_permissions_acces_eleve.md)


## QCM 5.1
Quelle commande linux permet de modifier le groupe propriétaire du dossier `livres` ?

- [ ] cat /etc/group
- [X] chown georges:laurent livres
- [ ] chmod 070 livres
- [ ] chown georges livres


## QCM 5.2
Un fichier `readme.md` possède les droits d'accès suivant :
```
-r--r--r-- 1 user2 NSI 354 juin 13 23:56 readme.md
```
Après l'exécution de la commande :
```
chmod 766 readme.md
```
Quelles sont les droits d'accès d'un utilisateur appartenant au groupe `NSI` sur le fichier `readme.md`  ?

- [X] Lire + Modifier
- [ ] Afficher + Exécuter + Supprimer
- [ ] Supprimer mais sans afficher le contenu
- [ ] Seulement lire  le contenu


## QCM 5.3
Un répertoire `recettes` possède les droits d'accès suivant :
```
drwxr-xr-- 2 eleve1 NSI 4096 juin  13 23:06 recettes
```
Parmi les affirmations suivantes, laquelle est vraie ?

- [ ] L'utilisateur `eleve1` ne peut pas lister les fichiers du répertoire
- [ ] Un utilisateur membre du groupe `NSI` peut créer un fichier dans ce répertoire
- [ ] Un utilisateur `user2` qui n'est pas membre du groupe `NSI` ne peut pas lister les fichiers du répertoire
- [X] L'utilisateur `eleve1` peut créer un fichier dans ce répertoire


## QCM 5.4
Un fichier script `auto.sh` possède les droits d'accès suivant :
```
-rwxr----- 1 eleve1 NSI 17 juin 13 23:14 auto.sh
```
Le fichier `/etc/group` contient (entre autre):
```
vboxsf:x:999:localuser,eleve1,user1,user2
NSI:x:1004:localuser,eleve1,user1
eleve1:x:1001:
SNT:x:1005:user2
```
Quelle commande linux donnera à l'utilisateur `user2` le droit d'exécuter le script `auto.sh`  ?

- [ ] `chown :SNT auto.sh`
- [ ] `chmod 744 auto.sh`
- [X] `chmod u+r,g+w,o+x auto.sh`
- [ ] `chmod u+rwx,g+rx-w,o+r-wx auto.sh`
