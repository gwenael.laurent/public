# TP Interpréteur de commandes Linux (présentation)
Auteur : Gwénaël LAURENT

## Sujets abordés
* `TP shell 1` : commandes de base en ligne de commande
* `TP shell 2` : [Droits et permissions d'accès aux fichiers](tp_shell_2_droits_permissions_acces_eleve.md)
* `QCM E3C` : Commandes de bases
* `QCM E3C` : [Droits d'accès aux fichiers](tp_shell_2_E3C.md)

# Objectifs  pédagogique
Programme NSI (classe de première) : rubrique `Architectures matérielles et systèmes d’exploitation` > `Systèmes d’exploitation` :
* Utiliser les commandes de base en ligne de commande
* Gérer les droits et permissions d'accès aux fichiers

## Pré-requis
* Notions de systèmes d'exploitation
* Fichiers / dossiers / programmes

# Préparation des postes de travail pour la NSI
## Machine virtuelle VirtualBox
* Un ordinateur par élève ou binôme avec un `OS Ubuntu Desktop` dans une machine virtuelle `Virtualbox`. Disque dur >= 20 Go. Presse papier et Glisser-Déposer activés en bidirectionnel. Dossier partagé avec la machine hôte (montage automatique permanent).

* Pour éviter de dupliquer la machine virtuelle sur le même poste et que les élèves puissent retrouver leur travail la prochaine séance, Ubuntu sera configuré avec plusieurs utilisateurs.

* Chaque utilisateur doit avoir accès au `shell` (bien sûr) mais aussi à `Thonny` (install en `sudo  pip3 install thonny` pour avoir la dernière version) et `VScode` (install en `sudo dpkg -i code_1.35...._amd64.deb` à partir du .deb téléchargé sur [code.visualstudio.com](code.visualstudio.com)).
Il faut ensuite éteindre puis redémarrer la machine virtuelle (un simple redémarrage semble ne pas suffir)

## Création des utilisateurs pour les élèves de NSI
Création d'un groupe pour les élèves de NSI :
```
% sudo groupadd NSI
```
Modification de la valeur de l'UMASK dans `/etc/login.defs` ligne 151
```
UMASK		027
```
Modifier également la valeur de l'UMASK dans `/etc/pam.d/common-session`  puis redémarrer Ubuntu:
```
session optional pam_umask.so umask=0027
```

Création des comptes élèves par : 
```
% sudo useradd eleve1 --create-home --groups NSI,vboxsf --gid NSI --shell /bin/bash
% sudo passwd eleve1
```

## Création de deux utilisateurs supplémentaires pour le TP shell 2 "Droits et permissions d'accès aux fichiers"
* `user1` **dans** le groupe `NSI`
```
% sudo useradd user1 --create-home --groups NSI,vboxsf --gid NSI --shell /bin/bash
% sudo passwd user1
```
* et `user2`  **PAS dans** le groupe `NSI`, mais dans le groupe `SNT`
```
% sudo groupadd SNT
% sudo useradd user2 --create-home --groups SNT,vboxsf --gid SNT --shell /bin/bash
% sudo passwd user2
```

# Quelques remarques techniques

## Augmenter la taille du disque virtuel après sa création
[modifier la taille du disque virtuel à 20 Go](http://derekmolloy.ie/resize-a-virtualbox-disk/) :
```
VBoxmanage modifyhd MyLinux.vdi --resize 20000
```
Puis dans Ubuntu, installer `*GParted` pour agrandir la taille de la partition.




