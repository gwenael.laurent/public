#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: TSP - Le voyageur de commerce ( Traveling Salesman Problem)
      Heuristique gloutonne du plus proche voisin (ppv)
:author: Gwénaël LAURENT
:date: 21 juin 2019

« Un voyageur de commerce doit visiter une et une seule fois un nombre
fini de villes et revenir à son point d’origine. Trouvez l’ordre de
visite des villes qui minimise la distance totale parcourue par le voyageur »

Heuristique = une méthode de calcul qui fournit rapidement une solution réalisable,
pas nécessairement optimale ou exacte, pour un problème d'optimisation difficile

Algorithme glouton = algorithme qui suit le principe de faire, étape par étape,
un choix optimum local. Dans certains cas cette approche permet d'arriver à un
optimum global, mais dans le cas général c'est une heuristique.

Dans la méthode du plus proche voisin, on part d'un sommet quelconque et à chacune
des (n-1) itérations on relie le dernier sommet atteint au sommet le plus proche
au sens coût, puis on relie finalement le dernier sommet au premier sommet choisi.

https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_voyageur_de_commerce
https://homepages.laas.fr/huguet/drupal/sites/homepages.laas.fr.huguet/files/u78/cours_Meta-4IR-2017-2018.pdf slide 40
https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/tsp/Readme.md
"""

import TSP_biblio

def get_liste_villes(fs) :
    """
    lecture fichier de ville format nomVille, longitude, latitude
    [['Annecy', 6.082499981, 45.8782196], ...]
    
    :param fs: (str) nom du fichier contenant les villes et coordonnées
    :return: (liste de liste) liste des villes avec leurs coordonnées
    
    >>> listeVilles = get_liste_villes("exemple.txt")
    
    """
    return TSP_biblio.get_tour_fichier(fs)
    

def genere_distances_villes(villes) :
    """
    génère une matrice qui stocke les distances 2 à 2 entre
    toutes les villes afin de ne faire le calcul de distance
    qu'une seule fois
    
    :param villes: (liste de liste) liste des villes avec leurs coordonnées
    :return: (liste de liste) matrice de distances entre les villes

    >>> listeVilles = get_liste_villes("exemple.txt")
    >>> m_dists = genere_distances_villes(listeVilles)
    
    """
    dists = []
    for i in range(0, len(villes)) :
        dist_une_ville = []
        for j in range(0, len(villes)) :
            dist_une_ville.append(TSP_biblio.distance(villes, i, j))
        dists.append(dist_une_ville)
    return dists
        
def ind_ville_plus_proche(ville_dep, listeVilles, distances) :
    """
    retourne l'indice de la ville la plus proche étant donnée une ville,
    une liste de ville sous forme d'indice,
    une matrice de distance
    
    :param ville_dep: (int) indice de la ville de départ
    :param listeVilles: (liste) liste des indices des villes à parcourir
                                sans la ville de départ/retour
    :param distances: (liste de liste) matrice des distances entre les villes
    :return (int) indice de la ville la plus proche parmi listeVilles
    
    >>> listeVilles = get_liste_villes("exemple.txt")
    >>> m_dists = genere_distances_villes(listeVilles)
    >>> ind_ville_plus_proche(8, [4,19], m_dists)
    4    
    """
    i = 0
    i_min = listeVilles[0]
    d_min = distances[ville_dep][i_min]
    while i < len(distances[ville_dep]) :
        d = distances[ville_dep][i]
        if (i != ville_dep) and (d < d_min) and i in listeVilles:
            i_min = i
            d_min = d
        i += 1
    return i_min

def trouver_chemin(ville_dep, listeVilles, distances) :
    """
    Heuristique gloutonne du plus proche voisin (ppv)
    donnant le tour parcouru par le voyageur
    de commerce à partir d'une ville donnée en paramètre,
    la liste des villes et la matrice de distance ville à ville
    
    :param ville_dep: (int) indice de la ville de départ
    :param listeVilles: (liste) liste des indices des villes à parcourir
                                sans la ville de départ/retour
    :param distances: (liste de liste) matrice des distances entre les villes
    :return: (liste) liste des indices des villes pour le tour + départ et arrivée
    """
    #une liste des villes du tour (à construire)
    listeIndTour = [ville_dep]
    #une liste des villes à placer dans le tour
    listeIndVillesRestantes = listeVilles[::]
    listeIndVillesRestantes.remove(ville_dep) # pour être sûr que la ville de départ n'appartiennt à listVilles
    
    #on parcours 1 à 1 les villes restantes
    indVilleCourante = ville_dep     
    while len(listeIndVillesRestantes) != 0 :
        #on cherche la ville la plus proche
        prochaineVille = ind_ville_plus_proche(indVilleCourante,
                                               listeIndVillesRestantes,
                                               distances)
        #on ajoute la ville la plus proche dans la liste du tour
        listeIndTour.append(prochaineVille)
        #on retire la ville déjà placée des villes restantes
        listeIndVillesRestantes.remove(prochaineVille)
        indVilleCourante = prochaineVille
    #on ajoute enfin le retour à la ville de départ
    listeIndTour.append(ville_dep)
    return listeIndTour
    

def get_parcours_villes (villes, listeIndTour) :
    """
    Génère une liste des villes du parcours sous la forme
    [['Annecy', 6.082499981, 45.8782196], ...]
    
    :param villes: (liste de liste) liste des villes avec leurs coordonnées
    :param listeIndTour: (liste) liste des indices des villes à parcourir
                                AVEC la ville de départ/retour
    :return: (liste de liste) liste des villes avec leurs coordonnées
    """
    return [ [villes[i][0], villes[i][1], villes[i][2]] for i in listeIndTour]

def get_id_ville(nomVille, villes) :
    """
    Retourne l'indice du nom de ville passé en paramètre dans la matrice villes
    
    :param nomVille: (str) le nom d'une ville
    :param villes: (liste de listes) une liste de ville
                                     telle que retourné par get_liste_villes
    :return: (int) l'indice de nomVille dans villes
                   si non trouvée, retourne 0
    :CU: nomVille doit faire partie de villes
    
    >>> get_id_ville('annecy', get_liste_villes("exemple.txt"))
    0
    """
    
    trouve = False
    id = 0
    while (id < len(villes)) and (trouve == False) :
       if nomVille.lower() in villes[id][0].lower() :
           trouve = True
       id += 1
    if trouve == False : return 0
    return id - 1


def afficherParcours(tour) :
    """
    Affiche le parcours avec pylab
    
    :param tour: (liste de liste) iste des villes avec leurs coordonnées
    :return: (None)
    """
    TSP_biblio.trace(tour)

if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    
    listeVilles = get_liste_villes("exemple.txt")
    m_dists = genere_distances_villes(listeVilles)
    """
    ind_ville_plus_proche(12, [2,3,4,5], m_dists) #4
    indTour = trouver_chemin(4, [6,10,20], m_dists) #[4, 10, 6, 20, 4]
    tour = get_parcours_villes(listeVilles, indTour)
    afficherParcours(tour)
    """
    #afficherParcours(get_parcours_villes(listeVilles, trouver_chemin(4, [i for i in range(0,len(listeVilles))], m_dists)))
    afficherParcours(get_parcours_villes(listeVilles,
                                         trouver_chemin(get_id_ville('nice', listeVilles),
                                                        [i for i in range(0,len(listeVilles))],
                                         m_dists)))