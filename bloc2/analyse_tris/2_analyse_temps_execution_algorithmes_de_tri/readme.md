# Analyse en temps d'exécution des tris
Auteur : Gwénaël LAURENT


# 1. Objectifs
L'objectif est d'`analyser` la performance en vitesse d'exécution des différents algorithmes de tri.

Pour cela, vous allez :
* générer et enregistrer des courbes de temps d'exécution.
* observer ces courbes et tirer des conclusions sur la vitesse d'exécution des différents tris

## Matériel fourni
Le TP précédent : Mesure et affichage du temps d'exécution des algorithmes de tri.

## Campagne d'expérimentation
Pour observer la vitesse d'exécution des tris, nous allons mener une campagne d'expérimentations qui va consister à comparer les temps d'exécution des tris en fonction de plusieurs critères.

# 2. Performance de chaque tri pour des types de listes différentes
Por chacun des 5 méthodes de tri, générez et enregistrer les courbes pour les différents types de listes (toujours pour taille maxi de listes de 500)
```python
>>> afficher_temps_exec_tous_types_listes("tri_select", 500)
>>> afficher_temps_exec_tous_types_listes("tri_insert", 500)
>>> afficher_temps_exec_tous_types_listes("tri_fusion", 500)
>>> afficher_temps_exec_tous_types_listes("tri_rapide", 500)
>>> afficher_temps_exec_tous_types_listes("tri_sort", 500)
```
![tri_select 500 toutes listes](./images/tri_select_500_tous_types_listes.png)
![tri_insert 500 toutes listes](./images/tri_insert_500_tous_types_listes.png)
![tri_fusion 500 toutes listes](./images/tri_fusion_500_tous_types_listes.png)
![tri_rapide 500 toutes listes](./images/tri_rapide_500_tous_types_listes.png)
![tri_sort 500 toutes listes](./images/tri_sort_500_tous_types_listes.png)

Que peut-on en déduire quant à la performance en temps d'exécution de ces tris ?

| Méthode de tri   | Sensible aux types de listes   | Meilleures performances pour quel type de listes  |Pires performances |
|:---------------  |:---------------------------:   | :-----------------------------------------------  |:------------      |
|Tri par sélection |    NON |   -                   |   -                    |
|Tri par insertion |    OUI |   Listes croissantes  |   Liste décroissantes  |
|Tri fusion        |    NON |   -                   |   -                    |
|Tri rapide        |    OUI |   Listes mélangées    |   Listes décroissantes |
|Tri sort          |    OUI |   Croissantes ou décroissantes | Mistes mélangées |

Bilan rapide : Que peut-on en déduire sur le choix d'un algorithme de tri ?
```
Que leur performance dépendra souvent du type de données à trier.
```

# 3. Comparer les tris pour des listes mélangées
Générez et enregistrer les courbes des 5 méthodes de tri pour des tailles maxi de listes de 50 et 500 (toujours avec des listes mélangées)
```python
>>> afficher_temps_exec_tous_les_tris(50, "cree_liste_melangee")
>>> afficher_temps_exec_tous_les_tris(500, "cree_liste_melangee")
```
![5 Tris 50 mélangées](./images/5_tris_50_melangees.png)
![5 Tris 500 mélangées](./images/5_tris_500_melangees.png)

Pour des `listes mélangée`s, classez les méthodes de tri par performance en temps d'exécution en fonction de la taille des données à classer.

**Pour des tailles de liste <30**

| Classement | Méthode de tri |
|:--------: |:-------------- |
| 1         |Tri sort       |
| 2         |Tri rapide     |
| 3         |Tri insertion  |
| 4         |Tri sélection  |
| 5         |Tri fusion     |

**Pour des tailles de liste >100**

| Classement | Méthode de tri |
|:---------:|:---------------|
| 1         |Tri sort       |
| 2         |Tri rapide     |
| 3         |Tri fusion     |
| 4         |Tri insertion  |
| 5         |Tri sélection  |

Bilan rapide : Que peut-on en déduire pour ce qui est du nombre de données à trier ?
```
Que la performance d'un algorithme de tri dépend souvent du nombre de données à trier. 
Par exemple, le tri fusion est plus performant sur de grandes quantités de données.
Certains algorithmes (comme le tri sort) semblent tout aussi performant sur de 
petites ou de grandes quantités de données.

```

# 4. Comparer les tris pour des listes déjà triés en ordre croissant
Générez et enregistrer les courbes des 5 méthodes de tri pour des tailles maxi de listes de 50 et 500 (toujours avec des listes triées en ordre croissant)
```python
>>> afficher_temps_exec_tous_les_tris(50, "cree_liste_croissante")
>>> afficher_temps_exec_tous_les_tris(500, "cree_liste_croissante")
```
![5 Tris 50 croissantes](./images/5_tris_50_croissantes.png)
![5 Tris 500 croissantes](./images/5_tris_500_croissantes.png)

Pour des `listes triées en ordre croissant`, classez les méthodes de tri par performance en temps d'exécution en fonction de la taille des données à classer.

**Pour des tailles de liste <30**

| Classement| Méthode de tri |
|:---------:|:---------------|
| 1         |Tri sort        |
| 2         |Tri insertion   |
| 3         |Tri rapide      |
| 4         |Tri sélection   |
| 5         |Tri fusion      |

**Pour des tailles de liste >100**

| Classement| Méthode de tri |
|:---------:|:---------------|
| 1         |Tri sort        |
| 2         |Tri insertion   |
| 3         |Tri fusion      |
| 4         |Tri rapide      |
| 5         |Tri sélection   |

Bilan rapide : Que peut-on en déduire si on veut trier en ordre croissant des données qui sont déjà presque triées en ordre croissant ?
```
Qu'un algorithme simple comme le tri insertion se révèle très rapide 
pour ce type de données.
Que le tri sort est toujours très performant.

```

# 5. Comparer les tris pour des listes déjà triés en ordre décroissant
Générez et enregistrer les courbes des 5 méthodes de tri pour des tailles maxi de listes de 50 et 500 (toujours avec des listes triées en ordre décroissant)
```python
>>> afficher_temps_exec_tous_les_tris(50, "cree_liste_decroissante")
>>> afficher_temps_exec_tous_les_tris(500, "cree_liste_decroissante")
```
![5 Tris 50 decroissantes](./images/5_tris_50_decroissantes.png)
![5 Tris 500 decroissantes](./images/5_tris_500_decroissantes.png)

Pour des `listes triées en ordre décroissant`, classez les méthodes de tri par performance en temps d'exécution en fonction de la taille des données à classer.

**Pour des tailles de liste <30**

| Classement| Méthode de tri |
|:---------:|:---------------|
| 1         |Tri sort        |
| 2         |Tri sélection   |
| 3         |Tri insertion   |
| 4         |Tri fusion      |
| 5         |Tri rapide      |

**Pour des tailles de liste >100**

| Classement| Méthode de tri |
|:---------:|:---------------|
| 1         |Tri sort        |
| 2         |Tri fusion      |
| 3         |Tri sélection   |
| 4         |Tri insertion   |
| 5         |Tri rapide      |

Bilan rapide : Que peut-on en déduire si on veut trier en ordre croissant des données qui sont déjà presque triées en ordre inverse ?
```
Que le tri sort est toujours très performant. 
Il semble insensible à l'ordre des données à trier.
Que le tri fusion se montre performant, tandis que 
les autres méthodes sont beaucoup plus lentes.
```

# 6. Et la palme est attribuée à ...
Vous avez analysé la performance de différents algorithmes de tri.
Vous vous êtes apperçu que la méthode de tri la plus performante est sans conteste de faire appel à la méthode `sort` du langage python.

La méthode `sort` de python utilise l'algorithme `Timsort`. De quels tris connus est-il le plus proche ?
```
https://fr.wikipedia.org/wiki/Timsort
Timsort est un algorithme de tri hybride dérivé du tri fusion et du tri par insertion, 
stable et conçu pour fonctionner de manière efficace sur des données réelles. 
Il a été mis au point par Tim Peters en 2002 pour le langage de programmation Python.
L'algorithme procède en cherchant des monotonies, c'est-à-dire des parties de 
l'entrée déjà correctement ordonnées, et peut de cette manière trier efficacement 
l'ensemble des données en procédant par fusions successives.
Pour des entrées de petites tailles, il revient à effectuer un tri fusion.
```
