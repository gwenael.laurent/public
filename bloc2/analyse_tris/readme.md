# Analyse des temps d'exécution des différents tris
Auteur : Gwénaël LAURENT

[_Travail pour le 17 juin 2019_](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/analyse_tris/Readme.md#pour-la-prochaine-fois-17-juin)

Deux parties :
1. [Mesure et affichage du temps d'exécution des algorithmes de tri](1_mesurer_temps_execution_des_algorithmes_de_tri/readme.md)
    * L'objectif est de `créer les outils logiciels` permettant d'analyser la performance en vitesse d'exécution des différentes méthodes de tri.
    * Modification du TP DIU EIL [Analyse en temps des algorithmes de tri](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/analyse_tris/Readme.md)
2. [Analyse en temps d'exécution des tris](2_analyse_temps_execution_algorithmes_de_tri/readme.md)
    * L'objectif est d'`analyser` la performance en vitesse d'exécution des différents algorithmes de tri.