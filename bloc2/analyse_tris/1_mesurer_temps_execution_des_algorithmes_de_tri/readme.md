# Mesure et affichage du temps d'exécution des algorithmes de tri
Auteur : Gwénaël LAURENT

Modification du TP DIU EIL [Analyse en temps des algorithmes de tri](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/analyse_tris/Readme.md)

# Objectifs
L'objectif est de créer les outils logiciels permettant d'analyser la `performance en vitesse d'exécution` des différentes méthodes de tri.

Pour cela, vous allez progressivement créer des fonctions utilitaires permettant de mesurer et d'afficher sous forme de courbes le temps d'exécution des différents algorithmes de tri.

## Matériel fourni
Récupérez et importez les deux modules [tris.py](./tris.py) et [listes.py](./listes.py)

# Mener une campagne d'expérimentation
Pour observer la vitesse d'exécution des tris, nous allons mener une campagne d'expérimentations qui va consister à :
* générer des listes de nombres :
    * listes de taille variable
    * listes contenant des nombres en ordre aléatoire (mélangées)
    * listes contenant des nombres déjà triés en ordre croissant
    * listes contenant des nombres dèjà triés en ordre décroissant 
* mesurer le temps d'exécution de chacun des tris pour ces listes.
* produire des courbes de temps d'exécution en fonction de la taille des listes à trier
* observer ces courbes et tirer des conclusions sur la vitesse d'exécution des différents tris 


# 1. Tri par sélection sur listes mélangées
On s'intéresse dans cette partie exclusivement au `tri par sélection` et au calcul du temps d'exécution sur des `listes mélangées`.

## 1.1 Tri par sélection sur des listes de 10 nombres
Réalisez une fonction sans paramètre `temps_exec_tri_selection_100_listes_taille_10` qui retournera le temps total mis pour trier 100 fois une liste mélangée de 10 nombres.

Cette fonction appellera donc 100 fois `tri_select(cree_liste_melangee(10))`

Pour le calcul du temps d'exécution, servez vous du module `timeit`présenté ci-après.

```python
>>> temps_exec_tri_selection_100_listes_taille_10()
0.0049467590000009665
```

### Mesure du temps avec le module `timeit`

Le module `timeit` de Python permet de prendre une mesure du temps
d'exécution d'une fonction, en secondes. Ce module fournit une
fonction `timeit` qui accepte en entrée trois paramètres :
* `setup` permet de préciser à `timeit` les modules à charger pour
  permettre l'exécution correcte de la fonction (y compris donc le
  module qui contient la fonction à mesurer),
* `stmt` – pour _statement_, instruction en anglais – est l'appel de
  fonction qui sera mesurée (donc avec ses paramètres), 
* `number` est le nombre de fois où l'instruction `stmt` sera
  exécutée. Le temps mesuré sera le temps cumulé pour toutes ces
  exécutions. 

Remarquez que le code Python des deux parmaètres `setup`et `stmt` sont
donnés sous forme d'une chaîne de caractères. 


Par exemple, après avoir importé le module `timeit` :
```python
from timeit import timeit
```

on peut mesurer le temps d'exécution du tri par sélection sur une liste mélangée de taille 10 :
```python
timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
       stmt='tri_select(cree_liste_melangee(10))',
       number=100)
```

## 1.2 Tri par sélection sur **une** liste de taille fixe
Réalisez une fonction `temps_exec_tri_selection_1_liste_taille_fixe` qui prend un paramètre `tailleListe` et qui retournera le temps total mis pour trier **une** liste mélangée contenant `tailleListe` nombres.

_Note : Vous utiliserez la méthode de chaine de caractères `format()`. Vous trouverez des exemples d'utilisation de cette méthode sur [docs.python.org](https://docs.python.org/fr/3/tutorial/inputoutput.html?highlight=chaine#the-string-format-method)_

```python
>>> temps_exec_tri_selection_1_liste_taille_fixe(1000)
0.2272137040000004
```

## 1.3 Tri par sélection sur **des** listes de tailles différentes
Réalisez une fonction `temps_exec_tri_selection_n_listes` qui prend un paramètre `tailleMaxListe` et qui **retourne une liste** des temps mis pour trier des listes mélangées de toutes les longueurs comprises entre 1 et `tailleMaxListe`.

```python
>>> temps_exec_tri_selection_n_listes(10)
[9.766999999882842e-06, 1.3366000000125666e-05, 1.3879999999133474e-05,
1.5421999999709612e-05, 2.004799999966167e-05, 2.261800000002978e-05,
2.57019999994057e-05, 2.878699999975254e-05, 3.289899999980861e-05,
3.804000000062757e-05]
```

## 1.4 Affichage des temps d'execution sur une courbe pylab
Réalisez une fonction `afficher_temps_exec_tri_selection_n_listes` qui prend un paramètre `tailleMaxListe` et qui **affiche avec pylab** les temps mis pour trier des listes mélangées de toutes les longueurs comprises entre 1 et `tailleMaxListe`.

```python
>>> afficher_temps_exec_tri_selection_n_listes(50)
```
![Tri sélection sur 50 listes mélangées](./images/tri_select_50.png)

### Aide pour tracer des courbes avec pylab
Étant donné un ensemble de valeurs dans une liste `l`, on peut utiliser le module `pylab` de matplotlib pour tracer une courbe où chaque valeur de la liste `l` à l'indice `i` est comprise comme le point de coordonnées `i`,`l[i]`.

Par exemple :
```python
import pylab
```
puis :
```python
l = [1,2,4,8,16]
pylab.plot(l)
pylab.show()
```

*Remarque :* en général, la méthode `show` de `pylab` ouvre une fenêtre pour présenter le graphique obtenu, et elle est bloquante, i.e. elle ne permet plus de dialoguer 
avec l'interpréteur Python. Il faut fermer cette fenêtre pour pouvoir poursuivre le dialogue. Avant la fermeture de cette fenêtre, certaines actions sont possibles sur 
le graphique. L'une d'elles est la sauvegarde du graphique dans un fichier image.


Si on veut que les abscisses soient différents, alors on fournit deux
listes : celles des abscisses et celle des ordonnées :
```python
x = [1,2,3,4,5]
y = [1,2,4,8,16]
pylab.plot(x,y)
pylab.show()
```

On peut améliorer la qualité du graphique produit en spécifiant des titres, et même une grille qui facilite la lecture  :
```python
NBRE_ESSAIS = 100
pylab.title('Temps du tri par sélection (pour {:d} essais)'.format(NBRE_ESSAIS))
pylab.xlabel('taille des listes')
pylab.ylabel('temps en secondes')
pylab.grid()
pylab.show()
```

# 2. Différentes méthodes de tri sur des listes mélangées
Dans cette partie, on continue d'utiliser des `listes mélangées`, mais vous allez mettre en oeuvre la mesure les temps d'exécution des autres méthodes de tri :
* `tri par sélection`
* `tri par insertion`
* `tri fusion`
* `tri rapide`

## 2.1 Choix de la méthode de tri sur **des** listes de tailles différentes
Réalisez une fonction `temps_exec_tri_n_listes` qui **retourne une liste** des temps mis pour trier des listes mélangées de toutes les longueurs comprises entre 1 et `tailleMaxListe` et qui prend deux paramètres :
* `methode_tri` pour le nom du tri à exécuter, sous forme d'une chaîne de caractères
* `tailleMaxListe`
 
_Note : N'oubliez pas de modifier le paramétrage de `timeit` pour importer seulement la méthode de tri choisie._

```python
>>> temps_exec_tri_n_listes("tri_fusion", 10)
[1.4393000000723077e-05, 2.5187999998621535e-05, 3.032899999766414e-05,
3.9582999999510093e-05, 4.780700000139859e-05, 5.706099999969183e-05,
6.682700000126829e-05, 7.71089999993535e-05, 8.584700000113799e-05,
9.561499999932721e-05]
```

## 2.2 Affichage des temps d'execution sur une courbe pylab
Réalisez une procédure `afficher_temps_exec_tri_n_listes` qui **affiche avec pylab** les temps mis pour trier des listes mélangées de toutes les longueurs comprises entre 1 et `tailleMaxListe` et qui prend deux paramètres :
* `methode_tri` pour le nom du tri à exécuter, sous forme d'une chaîne de caractères
* `tailleMaxListe`

```python
>>> afficher_temps_exec_tri_n_listes("tri_fusion", 50)
```
![Tri fusion sur 50 listes mélangées](./images/tri_fusion_50.png)


## 2.3 Affichage des temps d'execution de tous les tris sur un même graphique
Pour faciliter la comparaison des performances de chacun des tris, réalisez une procédure `afficher_temps_exec_tous_les_tris` qui prend en paramètre la taille maximale des listes à trier `tailleMaxListe`. Cette procédure **affiche sur un même graphique pylab** le temps d'exécution de tous les tris implémentés dans ce module.

> Vous trouverez de l'aide sur pylab et l'affichage de plusieurs courbes sur [www.courspython.com > Tracé de courbes](https://www.courspython.com/introduction-courbes.html)

_Note : fixer les couleurs de chacune des courbes_

```python
>>> afficher_temps_exec_tous_les_tris(50)
```
![4 tris sur 50 listes mélangées](./images/4_tris_50.png)


# 3. Différentes méthodes de tri sur différents types de listes
Jusqu'à maintenant, vous avez toujours utilisé des listes mélangées (données en ordre aléatoire) pour réaliser vos mesures de temps d'exécution. Il peut, en effet, _sembler_ logique de penser que c'est le cas le plus défavorable pour un algorithme de tri.

_**Mais qu'en est-il pour des données qui sont déjà presque triées ?**_

Vous allez donc réaliser des modifications dans vos fonctions de mesure et d'affichage pour prendre en compte différents types de listes. La création de ses listes utilisera les fonctions du module `listes.py` :
* listes mélangées (fonction `cree_liste_melangee`)
* listes déjà triée en ordre croissant (fonction `cree_liste_croissante`)
* listes déjà triée en ordre décroissant (fonction `cree_liste_decroissante`)


## 3.1 Modification de la fonction de calcul du temps
Modifiez la fonction `temps_exec_tri_n_listes` en lui ajoutant un paramètre `typeListe`qui désigne le type de liste à créer (valeur par défaut = "cree_liste_melangee"):
* "cree_liste_melangee"
* "cree_liste_croissante"
* "cree_liste_decroissante"

```python
>>> temps_exec_tri_n_listes("tri_fusion", 10, "cree_liste_croissante")
[3.5979992389911786e-06, 8.224999874073546e-06, 1.079600042430684e-05,
1.387999964208575e-05, 1.696399976935936e-05, 2.158999996026978e-05, 
2.5702999664645176e-05, 2.9300999813131057e-05, 3.444100002525374e-05, 
3.9582999306730926e-05]
```

## 3.2 Modification des procédures d'affichage
Modifiez les procédures d'affichage `afficher_temps_exec_tri_n_listes` et `afficher_temps_exec_tous_les_tris` en leur ajoutant un paramètre `typeListe`qui désigne le type de liste à créer (idem question précédente). La valeur par défaut de ce paramètre sera "cree_liste_melangee".

```python
>>> afficher_temps_exec_tri_n_listes("tri_rapide", 50, "cree_liste_croissante")
```
![tri rapide sur 50 listes croissantes](./images/tri_rapide_50_listes_croissantes.png)

```python
>>> afficher_temps_exec_tous_les_tris(50, "cree_liste_decroissante")
```
![4 tris sur 50 listes croissantes](./images/4_tris_50_listes_decroissantes.png)

## 3.3 Affichage des temps d'execution d'une méthode de tri sur différents types de listes
Pour faciliter la comparaison des performances d'un tri sur des types de listes différents, vous afficherez les temps d'exécution sur un même graphique pylab.

Réalisez une procédure `afficher_temps_exec_tous_types_listes` qui prend en paramètre une méthode de tri `methode_tri` et la taille maximale des listes à trier `tailleMaxListe`.

Cette procédure **affiche sur un même graphique pylab** les temps d'exécution pour chaque type de liste.

```python
>>> afficher_temps_exec_tous_types_listes("tri_insert", 50)
```
![Tri insert 50 Tous types listes](./images/tri_insert_50_tous_types_listes.png)


# 4. Ajout de la méthode `sort` de Python
Python possède une métode native `sort` pour trier des listes.
Intégrez ce 5ème type de tri pour produire les même courbes.
[Documentation sur la méthode sort](https://docs.python.org/fr/3/howto/sorting.html)

Exemple d'utilisation :
```python
>>> l = [0, 6, 3, 8, 7, 4, 1, 2, 5, 9]
>>> l.sort()
>>> print(l)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```
Vous allez devoir modifier les fonctions en ajoutant la méthode de tri `tri_sort`:
* `temps_exec_tri_n_listes`
* `afficher_temps_exec_tous_les_tris`

```python
>>> temps_exec_tri_n_listes("tri_sort", 10, "cree_liste_melangee")
[8.738000000008128e-06, 9.766999999882842e-06, 8.739000000090869e-06, 
1.1309999999653542e-05, 1.0794999999674815e-05, 1.1309999999653542e-05, 
1.1822999999466788e-05, 1.2850999999258761e-05, 1.4393000000723077e-05, 
1.593599999871742e-05]
```

```python
>>> afficher_temps_exec_tous_les_tris(10, "cree_liste_melangee")
```
![5 Tris 10 listes mélangées](./images/5_tris_10_listes_melangees.png)


