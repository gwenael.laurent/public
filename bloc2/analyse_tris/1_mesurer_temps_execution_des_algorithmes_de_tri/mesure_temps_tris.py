#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: mesure et affichage du temps d'exécution des algorithmes de tri
:author: Gwénaël LAURENT
:date: juin 2019

https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/analyse_tris/Readme.md
"""

import tris  
import listes  

from timeit import timeit  #pour mesurer le temps d'exécution d'une fonction
import pylab               #pour tracer des courbes

def temps_exec_tri_selection_100_listes_taille_10() :
    """
    Calcul du temps d'exécution du tri_select
    pour trier 100 fois une liste mélangée de 10 nombres

    :return: (float) temps d'éxécution
    :CU: n>0
    :effet de bord: (aucun)
    
    >>> type(temps_exec_tri_selection_100_listes_taille_10())
    <class 'float'>
    """


    tempsExec = timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
           stmt='tri_select(cree_liste_melangee(10))',
           number=100)
    return tempsExec

def temps_exec_tri_selection_1_liste_taille_fixe(tailleListe) :
    """
    Calcul du temps d'exécution du tri_select
    pour trier une liste mélangée
    de taille tailleListe
    
    :param tailleListe: (int) taille de la liste
    :return: (float) temps d'éxécution
    :CU: n>0
    :effet de bord: (aucun)
    
    >>> type(temps_exec_tri_selection_1_liste_taille_fixe(10))
    <class 'float'>
    """
    
    tempsExec = timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
           stmt='tri_select(cree_liste_melangee({}))'.format(tailleListe),
           number=1)
    return tempsExec

def temps_exec_tri_selection_n_listes(tailleMaxListe) :
    """
    Calcul du temps d'exécution du tri_select
    pour trier des listes mélangées
    de taille 1 à tailleMaxListe
    
    :param tailleMaxListe: (int) taille max des listes
    :return: (liste de float) temps d'éxécution pour chacune des taille de liste
    :CU: n>0
    :effet de bord: (aucun)
    
    >>> type(temps_exec_tri_selection_n_listes(10))
    <class 'list'>
    
    """
    temps = []
    for taille in range(1,tailleMaxListe+1) :
        tempsExec = timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
               stmt='tri_select(cree_liste_melangee({}))'.format(taille),
               number=1)
        temps.append(tempsExec)
    return temps

def afficher_temps_exec_tri_selection_n_listes(tailleMaxListe) :
    """
    Affiche avec pylab le temps d'exécution du tri_select
    pour trier des listes mélangées
    de taille 1 à tailleMaxListe

    :param tailleMaxListe: (int) taille max des listes
    :return: (NoneType) 
    :CU: n>0
    :effet de bord: (aucun)
    
    afficher_temps_exec_tri_selection_n_listes(50)
    
    """
    lTemps = temps_exec_tri_selection_n_listes(tailleMaxListe)
    lX = [ i  for i in range(1,tailleMaxListe+1) ]
    
    pylab.title('Temps d\'exécution tri sélection (pour {0:d} listes de taille 1 à {0:d})'.format(tailleMaxListe))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()    
    pylab.plot(lX,lTemps)
    pylab.show()

def temps_exec_tri_n_listes(methode_tri, tailleMaxListe, typeListe="cree_liste_melangee") :
    """
    Calcul du temps d'exécution d'une méthode de tri choisie
    pour trier des listes mélangées
    de taille 1 à tailleMaxListe
      
    :param methode_tri: (str) nom du tri à exécuter parmi "tri_select", "tri_insert", "tri_rapide", "tri_fusion" et "tri_sort"
    :param tailleMaxListe: (int) taille max des listes
    :parm typeListe: (str) nom de la fonction de création des listes de tests
                    * "cree_liste_melangee"
                    * "cree_liste_croissante"
                    * "cree_liste_decroissante"    
    :return: (liste de float) temps d'éxécution pour chacune des taille de liste
    :CU: n>0
    :effet de bord: (aucun)
    
    >>> type(temps_exec_tri_n_listes("tri_fusion", 10, "cree_liste_melangee"))
    <class 'list'>
    
    """
    temps = []
    if methode_tri != "tri_sort" :
        for taille in range(1,tailleMaxListe+1) :
            tempsExec = timeit(setup='from tris import {0}; from listes import {1}'.format(methode_tri, typeListe),
                   stmt='{0}({1}({2}))'.format(methode_tri, typeListe, taille),
                   number=1)
            temps.append(tempsExec)
    else :
        # Tri sort de python
        for taille in range(1,tailleMaxListe+1) :
            tempsExec = timeit(setup='from listes import {0}'.format(typeListe),
                   stmt='{0}({1}).sort()'.format(typeListe, taille),
                   number=1)
            temps.append(tempsExec)
    return temps

def afficher_temps_exec_tri_n_listes(methode_tri, tailleMaxListe, typeListe="cree_liste_melangee") :
    """
    Affiche avec pylab le temps d'exécution d'une méthode de tri choisie
    pour trier des listes créées par typeListe
    de taille 1 à tailleMaxListe
    
    :param methode_tri: (str) nom du tri à exécuter parmi "tri_select", "tri_insert", "tri_rapide", "tri_fusion"
    :param tailleMaxListe: (int) taille max des listes
    :parm typeListe: (str) nom de la fonction de création des listes de tests
                    * "cree_liste_melangee"
                    * "cree_liste_croissante"
                    * "cree_liste_decroissante" 
    :return: (NoneType) 
    :CU: n>0
    :effet de bord: (aucun)
    
    afficher_temps_exec_tri_n_listes("tri_select", 50, "cree_liste_melangee")
    afficher_temps_exec_tri_n_listes("tri_insert", 50)
    afficher_temps_exec_tri_n_listes("tri_fusion", 50)
    afficher_temps_exec_tri_n_listes("tri_rapide", 50, "cree_liste_croissante")

    """
    lTemps = temps_exec_tri_n_listes(methode_tri, tailleMaxListe, typeListe)
    lX = [ i  for i in range(1,tailleMaxListe+1) ]

    pylab.title('Temps d\'exécution {0} (pour {1:d} listes de taille 1 à {1:d})\n Fonction de création des listes : {2}'.format(methode_tri, tailleMaxListe, typeListe))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()    
    pylab.plot(lX,lTemps)
    pylab.show()


def afficher_temps_exec_tous_les_tris(tailleMaxListe, typeListe = "cree_liste_melangee") :
    """
    Affiche sur un même graphe pylab le temps d'exécution
    de tous les tris implémentés dans ce module
    pour trier des listes créées par typeListe
    de taille 1 à n
    
    :param tailleMaxListe: (int) taille max des listes
    :parm typeListe: (str) nom de la fonction de création des listes de tests
                    * "cree_liste_melangee"
                    * "cree_liste_croissante"
                    * "cree_liste_decroissante" 
    :return: (NoneType) 
    :CU: n>0
    :effet de bord: (aucun)
    
    afficher_temps_exec_tous_les_tris(10, "cree_liste_melangee")

    """
    lX = [ i  for i in range(1,tailleMaxListe+1) ]
    
    lT_select = temps_exec_tri_n_listes("tri_select", tailleMaxListe, typeListe)
    lT_insert = temps_exec_tri_n_listes("tri_insert", tailleMaxListe, typeListe)
    lT_fusion = temps_exec_tri_n_listes("tri_fusion", tailleMaxListe, typeListe)
    lT_rapide = temps_exec_tri_n_listes("tri_rapide", tailleMaxListe, typeListe)
    lT_sort = temps_exec_tri_n_listes("tri_sort", tailleMaxListe, typeListe)
    
    
    pylab.title('Temps d\'exécution des tris (pour {0:d} listes de taille 1 à {0:d})\n Fonction de création des listes : {1}'.format(tailleMaxListe, typeListe))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    
    pylab.plot(lX, lT_select, "r-", label="tri sélection")
    pylab.plot(lX, lT_insert, "c-", label="tri insertion")
    pylab.plot(lX, lT_fusion, "b-", label="tri fusion")
    pylab.plot(lX, lT_rapide, "g-", label="tri rapide")
    pylab.plot(lX, lT_sort, "m-", label="tri sort")

    pylab.legend()
    pylab.show()

def afficher_temps_exec_tous_types_listes(methode_tri, tailleMaxListe) :
    """
    Affiche sur un même graphe pylab le temps d'exécution
    d'une méthode de tri 
    pour trier tous les types de listes
    de taille 1 à n
    
    Types de listes :
     * "cree_liste_melangee",
     * "cree_liste_croissante",
     * "cree_liste_decroissante"
    
    :param methode_tri: (str) nom du tri à exécuter parmi "tri_select", "tri_insert", "tri_rapide", "tri_fusion" et "tri_sort"
    :param tailleMaxListe: (int) taille max des listes

    :return: (NoneType) 
    :CU: n>0
    :effet de bord: (aucun)
    
    afficher_temps_exec_tous_types_listes("tri_insert", 50)

    """
    
    lX = [ i  for i in range(1,tailleMaxListe+1) ]
    
    lT_melangee = temps_exec_tri_n_listes(methode_tri, tailleMaxListe, "cree_liste_melangee")
    lT_croissante = temps_exec_tri_n_listes(methode_tri, tailleMaxListe, "cree_liste_croissante")
    lT_decroissante = temps_exec_tri_n_listes(methode_tri, tailleMaxListe, "cree_liste_decroissante")   
    
    pylab.title('Temps d\'exécution {0} (pour {1:d} listes de taille 1 à {1:d})'.format(methode_tri, tailleMaxListe))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    
    pylab.plot(lX, lT_melangee, "r-", label="listes mélangées")
    pylab.plot(lX, lT_croissante, "c-", label="listes croissantes")
    pylab.plot(lX, lT_decroissante, "b-", label="listes décroissantes")

    pylab.legend()
    pylab.show()


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)

    #lc = cree_liste_croissante(10)
    #ld = cree_liste_decroissante(10)
    #lm1 = cree_liste_melangee(10)
    #lm2 = cree_liste_melangee2(10, 5)
    #
    #
    #tempsExec = timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
    #       stmt='tri_select(cree_liste_melangee(10))',
    #       number=100)
    #print(tempsExec)
    #afficher_temps_exec_pour_un_tri("tri_rapide", 10)
    #afficher_temps_exec_tous_les_tris(10)
    
    #afficher_temps_exec_tri_n_listes("tri_select", 50)
    #afficher_temps_exec_tri_n_listes("tri_insert", 50)
    #afficher_temps_exec_tri_n_listes("tri_fusion", 50)
    #afficher_temps_exec_tri_n_listes("tri_rapide", 50)
