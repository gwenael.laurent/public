# Ressources pour la programmation NodeJS

> * Auteur : Gwénaël LAURENT
> * Date : 13/04/2020
> * OS : Windows 10 (version 1903)
> * VScode : version 1.43.0 (system setup)
> * Node.js : version v12.16.2

![CC-BY-NC-SA](../img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

![NodeJS logo](img/nodejs-logo.png)

* [Installation de NodeJS](installer_nodejs.md)
